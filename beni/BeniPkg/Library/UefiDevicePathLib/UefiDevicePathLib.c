/**
*  @Package     : BeniPkg
*  @FileName    : UefiDevicePathLib.c
*  @Date        : 20230923
*  @Author      : Jiangwei
*  @Version     : 1.0
*  @Description :
*    This is a library for deivce path.
*
*  @History:
*    20230923: Initialize.
*
*  This program and the accompanying materials
*  are licensed and made available under the terms and conditions of the BSD License
*  which accompanies this distribution. The full text of the license may be found at
*  http://opensource.org/licenses/bsd-license.php
*
*  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
*  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
**/

#include <Uefi.h>

#include <Library/UefiBootServicesTableLib.h>
#include <Library/DebugLib.h>
#include <Library/UefiLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/DevicePathLib.h>
#include <Library/BeniDevicePathLib.h>

/**
  Print device path.

  @param[in]  DevicePath            The device path protocol.

  @retval  NA

**/
VOID
EFIAPI
BeniPrintDevicePath (
  IN  EFI_DEVICE_PATH_PROTOCOL      *DevicePath
  )
{
  CHAR16  *Temp = NULL;

  Temp = ConvertDevicePathToText (DevicePath, TRUE, TRUE);
  Print (L"DevPath: %s\r\n", Temp);

  if (NULL != Temp) {
    FreePool (Temp);
    Temp = NULL;
  }
}
