##
#  @Package     : BeniPkg
#  @FileName    : BeniLibs.dsc.inc
#  @Date        : 20230225
#  @Author      : Jiangwei
#  @Version     : 1.0
#  @Description :
#    This file contains components for learning BIOS.
#
#  @History:
#    20230225: Initialize.
#
#  This program and the accompanying materials
#  are licensed and made available under the terms and conditions of the BSD License
#  which accompanies this distribution. The full text of the license may be found at
#  http://opensource.org/licenses/bsd-license.php
#
#  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
#  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
##

[LibraryClasses]
  BeniAsmLib|BeniPkg/Library/BaseAsmLib/BaseAsmLib.inf
  BeniDebugInfoLib|BeniPkg/Library/BaseDebugInfoLib/BaseDebugInfoLib.inf
  BeniGlobalDataTestLib|BeniPkg/Library/DxeGlobalDataTestLib/DxeGlobalDataTestLib.inf
  BeniDevicePathLib|BeniPkg/Library/UefiDevicePathLib/UefiDevicePathLib.inf
  BeniHiiLib|BeniPkg/Library/UefiHiiLib/UefiHiiLib.inf
  BeniTimeLib|BeniPkg/Library/UefiTimeLib/UefiTimeLib.inf
!if $(REDFISH_ENABLE) == TRUE
  RedfishPlatformHostInterfaceLib|EmulatorPkg/Library/RedfishPlatformHostInterfaceLib/RedfishPlatformHostInterfaceLib.inf
  RedfishPlatformCredentialLib|EmulatorPkg/Library/RedfishPlatformCredentialLib/RedfishPlatformCredentialLib.inf
!endif
  ShellCommandLib|ShellPkg/Library/UefiShellCommandLib/UefiShellCommandLib.inf
  #
  # This is for Redifsh.
  #
  IpmiLib|MdeModulePkg/Library/BaseIpmiLibNull/BaseIpmiLibNull.inf
  IpmiCommandLib|MdeModulePkg/Library/BaseIpmiCommandLibNull/BaseIpmiCommandLibNull.inf

[LibraryClasses.common.PEIM]

[LibraryClasses.common.DXE_DRIVER]
