/**
*  @Package     : BeniPkg
*  @FileName    : HansFont.c
*  @Date        : 20230704
*  @Author      : Jiangwei
*  @Version     : 1.0
*  @Description :
#    Install Hans font.
*
*  @History:
*    20230704: Initialize.
*
*  This program and the accompanying materials
*  are licensed and made available under the terms and conditions of the BSD License
*  which accompanies this distribution. The full text of the license may be found at
*  http://opensource.org/licenses/bsd-license.php
*
*  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
*  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
**/

#include <Uefi.h>

#include <Library/UefiDriverEntryPoint.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/BaseLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DebugLib.h>
#include <Library/HiiLib.h>

extern EFI_NARROW_GLYPH mWideGlyphData[];
extern UINT32           mWideFontSize;

EFI_HII_HANDLE          mHiiHandle = NULL;
// {FDC9D148-923A-4AEE-9D4C-B78A3E82792F}
STATIC CONST EFI_GUID   mFontPackageListGuid = {
  0xfdc9d148, 0x923a, 0x4aee, { 0x9d, 0x4c, 0xb7, 0x8a, 0x3e, 0x82, 0x79, 0x2f }
};

/**
  Main entry of the driver.

  @param[in]  ImageHandle           Image handle this driver.
  @param[in]  SystemTable           Pointer to the System Table.

  @retval  EFI_SUCCESS              Driver executed successfully.
  @retval  Others                   Error happened.

**/
EFI_STATUS
EFIAPI
HansFontEntry (
  IN  EFI_HANDLE                    ImageHandle,
  IN  EFI_SYSTEM_TABLE              *SystemTable
  )
{
  EFI_STATUS                      Status = EFI_ABORTED;
  UINT32                          PackageLength = 0;
  UINT8                           *Package = NULL;
  EFI_HII_SIMPLE_FONT_PACKAGE_HDR *SimplifiedFont = NULL;
  UINT8                           *Location = NULL;

  //
  // Reference:
  // edk2\MdeModulePkg\Universal\Console\GraphicsConsoleDxe\GraphicsConsole.c
  //
  // Add 4 bytes to the header for entire length for HiiAddPackages use only.
  //
  //    +--------------------------------+ <-- Package
  //    |                                |
  //    |    PackageLength(4 bytes)      |
  //    |                                |
  //    |--------------------------------| <-- SimplifiedFont
  //    |                                |
  //    |EFI_HII_SIMPLE_FONT_PACKAGE_HDR |
  //    |                                |
  //    |--------------------------------| <-- Location
  //    |                                |
  //    |     gUsStdNarrowGlyphData      |
  //    |                                |
  //    +--------------------------------+
  //
  PackageLength = sizeof (EFI_HII_SIMPLE_FONT_PACKAGE_HDR) + mWideFontSize + 4;
  Package       = AllocateZeroPool (PackageLength);
  if (NULL == Package) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Out of memory\n", __FUNCTION__, __LINE__));
    return EFI_OUT_OF_RESOURCES;
  }

  //
  // Without this code, system will hang.
  //
  WriteUnaligned32 ((UINT32 *)Package, PackageLength);

  SimplifiedFont                        = (EFI_HII_SIMPLE_FONT_PACKAGE_HDR *)(Package + 4);
  SimplifiedFont->Header.Length         = (UINT32)(PackageLength - 4);
  SimplifiedFont->Header.Type           = EFI_HII_PACKAGE_SIMPLE_FONTS;
  SimplifiedFont->NumberOfNarrowGlyphs  = 0;
  SimplifiedFont->NumberOfWideGlyphs    = (UINT16)(mWideFontSize / sizeof (EFI_WIDE_GLYPH));

  Location = (UINT8 *)(&SimplifiedFont->NumberOfWideGlyphs + 1);
  CopyMem (Location, mWideGlyphData, mWideFontSize);

  mHiiHandle = HiiAddPackages (
                &mFontPackageListGuid,
                NULL,
                Package,
                NULL
                );
  if (NULL == mHiiHandle) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] NULL == mHiiHandle\n", __FUNCTION__, __LINE__));
    Status = EFI_NOT_READY;
  } else {
    DEBUG ((EFI_D_ERROR, "HanFont added\n"));
  }

  FreePool (Package);

  return Status;
}
