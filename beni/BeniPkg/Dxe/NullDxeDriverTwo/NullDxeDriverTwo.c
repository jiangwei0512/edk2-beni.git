/**
*  @Package     : BeniPkg
*  @FileName    : NullDxeDriverTwo.c
*  @Date        : 20180616
*  @Author      : Jiangwei
*  @Version     : 1.0
*  @Description :
*    This is used to test global data in library.
*    This is a bad example.
*
*  @History:
*    20180616: Initialize.
*
*  This program and the accompanying materials
*  are licensed and made available under the terms and conditions of the BSD License
*  which accompanies this distribution. The full text of the license may be found at
*  http://opensource.org/licenses/bsd-license.php
*
*  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
*  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
**/

#include <Uefi.h>

#include <Library/DebugLib.h>
#include <Library/UefiDriverEntryPoint.h>
#include <Library/UefiBootServicesTableLib.h>

#include <Library/BeniGlobalDataTestLib.h>

/**
  Main entry of the driver.

  @param[in]  ImageHandle           Image handle of this driver.
  @param[in]  SystemTable           Pointer to the System Table.

  @retval  EFI_SUCCESS              Driver executed successfully.
  @retval  Others                   Error happened.

**/
EFI_STATUS
EFIAPI
NullDxeDriverTwoEntry (
  IN  EFI_HANDLE                    ImageHandle,
  IN  EFI_SYSTEM_TABLE              *SystemTable
  )
{
  DEBUG ((EFI_D_ERROR, "[%a] SystemTable: 0x%p\n", __FUNCTION__, SystemTable));
  DEBUG ((EFI_D_ERROR, "[%a] gBS        : 0x%p\n", __FUNCTION__, gBS));
  BeniPrintGlobalVar ();
  BeniPrintGlobalVar2 ();

  return EFI_SUCCESS;
}
