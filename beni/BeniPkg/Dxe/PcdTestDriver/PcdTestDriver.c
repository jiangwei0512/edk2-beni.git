/**
*  @Package     : BeniPkg
*  @FileName    : PcdTestDriver.c
*  @Date        : 20211004
*  @Author      : Jiangwei
*  @Version     : 1.0
*  @Description :
*    This driver is used to do some tests about PCDs.
*
*  @History:
*    20211004: Initialize.
*
*  This program and the accompanying materials
*  are licensed and made available under the terms and conditions of the BSD License
*  which accompanies this distribution. The full text of the license may be found at
*  http://opensource.org/licenses/bsd-license.php
*
*  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
*  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
**/

#include <Uefi.h>

#include <Library/UefiDriverEntryPoint.h>
#include <Library/DebugLib.h>
#include <Library/PcdLib.h>

/**
  Main entry of the driver.

  @param[in]  ImageHandle           Image handle this driver.
  @param[in]  SystemTable           Pointer to the System Table.

  @retval  EFI_SUCCESS              Driver executed successfully.
  @retval  Others                   Error happened.

**/
EFI_STATUS
EFIAPI
PcdTestDriverEntry (
  IN  EFI_HANDLE                    ImageHandle,
  IN  EFI_SYSTEM_TABLE              *SystemTable
  )
{
  DEBUG ((EFI_D_ERROR, "[%a] PcdTestVar1      : 0x%x\n", __FUNCTION__, PcdGet8 (PcdTestVar1)));
  DEBUG ((EFI_D_ERROR, "[%a] PcdTestVar2      : 0x%x\n", __FUNCTION__, PcdGet16 (PcdTestVar2)));
  DEBUG ((EFI_D_ERROR, "[%a] PcdTestVar3      : 0x%x\n", __FUNCTION__, PcdGet32 (PcdTestVar3)));
  DEBUG ((EFI_D_ERROR, "[%a] PcdTestVar4      : 0x%x\n", __FUNCTION__, PcdGet64 (PcdTestVar4)));
  DEBUG ((EFI_D_ERROR, "[%a] PcdTestFeatureVar: %d\n",   __FUNCTION__, FeaturePcdGet (PcdTestFeatureVar)));
  DEBUG ((EFI_D_ERROR, "[%a] PcdTestBooleanVar: %d\n",   __FUNCTION__, PcdGetBool (PcdTestFeatureVar)));
  DEBUG ((EFI_D_ERROR, "[%a] PcdTestVersion   : 0x%x\n", __FUNCTION__, PcdGet32 (PcdTestVersion)));
  DEBUG ((EFI_D_ERROR, "[%a] PcdPatchableVar  : 0x%x\n", __FUNCTION__, PcdGet32 (PcdPatchableVar)));

  return EFI_SUCCESS;
}
