/**
*  @Package     : BeniPkg
*  @FileName    : HobConsumer.c
*  @Date        : 20230304
*  @Author      : Jiangwei
*  @Version     : 1.0
*  @Description :
*    Check HOB created in PEI stage.
*
*  @History:
*    20230304: Initialize.
*
*  This program and the accompanying materials
*  are licensed and made available under the terms and conditions of the BSD License
*  which accompanies this distribution. The full text of the license may be found at
*  http://opensource.org/licenses/bsd-license.php
*
*  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
*  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
**/

#include <PiDxe.h>
#include <BeniData.h>

#include <Library/UefiDriverEntryPoint.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/DebugLib.h>
#include <Library/HobLib.h>

/**
  Main entry of the driver.

  @param[in]  ImageHandle           Image handle for this driver.
  @param[in]   SystemTable          Pointer to the System Table.

  @retval  EFI_SUCCESS              Driver executed successfully.
  @retval  Others                   Error happened.

**/
EFI_STATUS
EFIAPI
HobConsumerEntry (
  IN  EFI_HANDLE                    ImageHandle,
  IN  EFI_SYSTEM_TABLE              *SystemTable
  )
{
  EFI_PEI_HOB_POINTERS  Hob;
  UINTN                 Index = 0;
  UINT8                 *Data = NULL;

  Hob.Raw = GetFirstGuidHob (&gBeniHobGuid);
  if (NULL == Hob.Raw) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Failed. - %r\n", __FUNCTION__, __LINE__, EFI_NOT_FOUND));
    return EFI_NOT_FOUND;
  }

  Data = (UINT8 *)(GET_GUID_HOB_DATA (Hob.Raw));
  DEBUG ((EFI_D_ERROR, "BENI Hob data:\n"));
  for (Index = 0; Index < BENI_HOB_DATA_LEN; Index++) {
    DEBUG ((EFI_D_ERROR, "0x%02x ", *(Data + Index)));
  }
  DEBUG ((EFI_D_ERROR, "\n"));

  return EFI_SUCCESS;
}
