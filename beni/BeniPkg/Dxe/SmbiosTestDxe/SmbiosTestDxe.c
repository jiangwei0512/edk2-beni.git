/**
*  @Package     : BeniPkg
*  @FileName    : SmbiosTestDxe.c
*  @Date        : 20240904
*  @Author      : Jiangwei
*  @Version     : 1.0
*  @Description :
*    This driver is used to test SMBIOS interfaces.
*
*  @History:
*    20240904: Initialize.
*
*  This program and the accompanying materials
*  are licensed and made available under the terms and conditions of the BSD License
*  which accompanies this distribution. The full text of the license may be found at
*  http://opensource.org/licenses/bsd-license.php
*
*  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
*  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
**/

#include <Uefi.h>

#include <Library/UefiDriverEntryPoint.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>

#include <Protocol/Smbios.h>

SMBIOS_TABLE_TYPE11  gSmbiosType11Template = {
  { EFI_SMBIOS_TYPE_OEM_STRINGS, sizeof (SMBIOS_TABLE_TYPE11), 0 },
  1 // StringCount
};

CHAR8 *gSmbiosType11Strings = "Hello BIOS";

/**
  Main entry of the driver.

  @param[in]  ImageHandle           Image handle this driver.
  @param[in]  SystemTable           Pointer to the System Table.

  @retval  EFI_SUCCESS              Driver executed successfully.
  @retval  Others                   Error happened.

**/
EFI_STATUS
EFIAPI
SmbiosTestDxeEntry (
  IN  EFI_HANDLE                    ImageHandle,
  IN  EFI_SYSTEM_TABLE              *SystemTable
  )
{
  EFI_STATUS              Status = EFI_ABORTED;
  EFI_SMBIOS_PROTOCOL     *Smbios = NULL;
  UINTN                   StringSize = 0;
  UINTN                   Size = 0;
  EFI_SMBIOS_TABLE_HEADER *Record = NULL;
  CHAR8                   *Str = NULL;
  EFI_SMBIOS_HANDLE       SmbiosHandle = 0;
  EFI_SMBIOS_TYPE         Type = 0;
  EFI_SMBIOS_TABLE_HEADER *Header = NULL;
  SMBIOS_TABLE_TYPE0      *Type0 = NULL;
  UINTN                   StringIndex = 0;

  Status = gBS->LocateProtocol (
                  &gEfiSmbiosProtocolGuid,
                  NULL,
                  (VOID **)(&Smbios)
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Failed. - %r\n", __FUNCTION__, __LINE__, Status));
    return Status;
  }

  Size = gSmbiosType11Template.Hdr.Length;
  StringSize = AsciiStrSize (gSmbiosType11Strings);
  Size += StringSize;
  Size += 1;

  Record = (EFI_SMBIOS_TABLE_HEADER *)(AllocateZeroPool (Size));
  if (NULL == Record) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Out of memory\n", __FUNCTION__, __LINE__));
    return EFI_OUT_OF_RESOURCES;
  }

  CopyMem (Record, &gSmbiosType11Template, gSmbiosType11Template.Hdr.Length);
  Str = ((CHAR8 *)Record) + Record->Length;
  CopyMem (Str, gSmbiosType11Strings, StringSize);

  SmbiosHandle = SMBIOS_HANDLE_PI_RESERVED;
  Status       = Smbios->Add (
                          Smbios,
                          NULL,
                          &SmbiosHandle,
                          Record
                          );
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Failed. - %r\n", __FUNCTION__, __LINE__, Status));
  }

  SmbiosHandle = SMBIOS_HANDLE_PI_RESERVED;
  Type = SMBIOS_TYPE_BIOS_INFORMATION;
  Status = Smbios->GetNext (
                    Smbios,
                    &SmbiosHandle,
                    &Type,
                    &Header,
                    NULL
                    );
  if (!EFI_ERROR (Status)) {
    Type0 = (SMBIOS_TABLE_TYPE0 *)Header;
    DEBUG ((EFI_D_ERROR, "Vendor Index: %d\n", Type0->Vendor));
    Str = ((CHAR8 *)Type0) + Type0->Hdr.Length;
    DEBUG ((EFI_D_ERROR, "Vendor: %a\n", Str));
    Str += AsciiStrSize (Str);
    DEBUG ((EFI_D_ERROR, "BiosVersion: %a\n", Str));

    StringIndex = Type0->BiosVersion;
    Status = Smbios->UpdateString (
                      Smbios,
                      &SmbiosHandle,
                      &StringIndex,
                      "V1.0.0"
                      );
  }

  return Status;
}
