/**
*  @Package     : BeniPkg
*  @FileName    : TcpTransportDxe.c
*  @Date        : 20231228
*  @Author      : Jiangwei
*  @Version     : 1.0
*  @Description :
*    This driver installs TCP transport protocol.
*    Base on EmbeddedPkg/Drivers/AndroidFastbootTransportTcpDxe/FastbootTransportTcpDxe.inf.
*
*  @History:
*    20231228: Initialize.
*
*  This program and the accompanying materials
*  are licensed and made available under the terms and conditions of the BSD License
*  which accompanies this distribution. The full text of the license may be found at
*  http://opensource.org/licenses/bsd-license.php
*
*  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
*  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
**/

/** @file
#
#  Copyright (c) 2014, ARM Ltd. All rights reserved.<BR>
#
#  SPDX-License-Identifier: BSD-2-Clause-Patent
#
#
#**/

#include <Uefi.h>

#include <Library/UefiDriverEntryPoint.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DebugLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/PrintLib.h>

#include <Protocol/Dhcp4.h>
#include <Protocol/Tcp4.h>
#include <Protocol/ServiceBinding.h>
#include <Protocol/SimpleTextOut.h>
#include <Protocol/TcpTransportProtocol.h>

#define IP4_ADDR_TO_STRING(IpAddr, IpAddrString)  UnicodeSPrint (      \
                                                    IpAddrString,       \
                                                    16 * 2,             \
                                                    L"%d.%d.%d.%d",     \
                                                    IpAddr.Addr[0],     \
                                                    IpAddr.Addr[1],     \
                                                    IpAddr.Addr[2],     \
                                                    IpAddr.Addr[3]      \
                                                    );

//
// (This isn't actually a packet size - it's just the size of the buffers we
// pass to the TCP driver to fill with received data.)
// We can achieve much better performance by doing this in larger chunks.
//
#define RX_FRAGMENT_SIZE  2048
#define MAX_BUFFER_SIZE   512
//
// We only ever use one IO token for receive and one for transmit. To save
// repeatedly allocating and freeing, just allocate statically and re-use.
//
#define NUM_RX_TOKENS       16
#define TOKEN_NEXT(Index)   (((Index) + 1) % NUM_RX_TOKENS)

STATIC EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL  *mTextOut = NULL;
STATIC EFI_TCP4_PROTOCOL                *mTcpConnection = NULL;
STATIC EFI_TCP4_PROTOCOL                *mTcpListener = NULL;
STATIC EFI_EVENT                        mReceiveEvent = NULL;
STATIC EFI_SERVICE_BINDING_PROTOCOL     *mTcpServiceBinding = NULL;
STATIC EFI_HANDLE                       mTcpHandle = NULL;
STATIC UINTN                            mNextSubmitIndex = 0;
STATIC UINTN                            mNextReceiveIndex = 0;
STATIC EFI_TCP4_IO_TOKEN                mReceiveToken[NUM_RX_TOKENS];
STATIC EFI_TCP4_RECEIVE_DATA            mRxData[NUM_RX_TOKENS];
STATIC EFI_TCP4_IO_TOKEN                mTransmitToken;
STATIC EFI_TCP4_TRANSMIT_DATA           mTxData;
//
// We also reuse the accept token
//
STATIC EFI_TCP4_LISTEN_TOKEN            mAcceptToken;
//
// .. and the close token
//
STATIC EFI_TCP4_CLOSE_TOKEN             mCloseToken;
STATIC LIST_ENTRY                       mPacketListHead;

//
// List type for queued received packets.
//
typedef struct _TCP_PACKET_LIST {
  LIST_ENTRY  Link;
  VOID        *Buffer;
  UINTN       BufferSize;
} TCP_PACKET_LIST;

/*
  Helper function to set up a receive IO token and call Tcp->Receive

  @param  NA

  @retval  EFI_SUCCESS              Toke created.
  @retval  Others                   Toke not created.

*/
STATIC
EFI_STATUS
SubmitRecieveToken (
  VOID
  )
{
  EFI_STATUS  Status = EFI_SUCCESS;
  VOID        *FragmentBuffer = NULL;

  FragmentBuffer = AllocateZeroPool (RX_FRAGMENT_SIZE);
  if (NULL == FragmentBuffer) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Out of memory\n", __FUNCTION__, __LINE__));
    return EFI_OUT_OF_RESOURCES;
  }

  mRxData[mNextSubmitIndex].DataLength                      = RX_FRAGMENT_SIZE;
  mRxData[mNextSubmitIndex].FragmentTable[0].FragmentLength = RX_FRAGMENT_SIZE;
  mRxData[mNextSubmitIndex].FragmentTable[0].FragmentBuffer = FragmentBuffer;

  Status = mTcpConnection->Receive (mTcpConnection, &mReceiveToken[mNextSubmitIndex]);
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "TCP receive failed - %r\n", Status));
    FreePool (FragmentBuffer);
  }

  mNextSubmitIndex = TOKEN_NEXT (mNextSubmitIndex);
  return Status;
}

/*
  Event notify function for when we have closed our TCP connection.
  We can now start listening for another connection.

  @param[in]  Event                 The event.
  @param[in]  Context               The context of event.

  @retval  NA

*/
STATIC
VOID
ConnectionClosed (
  IN  EFI_EVENT                     Event,
  IN  VOID                          *Context
  )
{
  EFI_STATUS  Status = EFI_ABORTED;

  // Possible bug in EDK2 TCP4 driver: closing a connection doesn't remove its
  // PCB from the list of live connections. Subsequent attempts to Configure()
  // a TCP instance with the same local port will fail with INVALID_PARAMETER.
  // Calling Configure with NULL is a workaround for this issue.
  Status = mTcpConnection->Configure (mTcpConnection, NULL);

  mTcpConnection = NULL;

  Status = mTcpListener->Accept (mTcpListener, &mAcceptToken);
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "TCP accept: %r\n", Status));
  }
}

/*
  Close all events.

  @param  NA

  @retval  NA

*/
STATIC
VOID
CloseReceiveEvents (
  VOID
  )
{
  UINTN Index = 0;

  for (Index = 0; Index < NUM_RX_TOKENS; Index++) {
    gBS->CloseEvent (mReceiveToken[Index].CompletionToken.Event);
  }
}

/*
  Event notify function to be called when we receive TCP data.

  @param[in]  Event                 The event.
  @param[in]  Context               The context of event.

  @retval  NA

*/
STATIC
VOID
EFIAPI
DataReceived (
  IN  EFI_EVENT                     Event,
  IN  VOID                          *Context
  )
{
  EFI_STATUS        Status = EFI_ABORTED;
  TCP_PACKET_LIST   *NewEntry = NULL;
  EFI_TCP4_IO_TOKEN *ReceiveToken = NULL;

  ReceiveToken = &mReceiveToken[mNextReceiveIndex];

  Status = ReceiveToken->CompletionToken.Status;
  if (EFI_CONNECTION_FIN == Status) {
    //
    // Remote host closed connection. Close our end.
    //
    CloseReceiveEvents ();

    Status = mTcpConnection->Close (mTcpConnection, &mCloseToken);
    if (EFI_ERROR (Status)) {
      DEBUG ((EFI_D_ERROR, "TCP close failed - %r\n", Status));
    }

    return;
  }

  //
  // Add an element to the receive queue.
  //
  NewEntry = AllocateZeroPool (sizeof (TCP_PACKET_LIST));
  if (NULL == NewEntry) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Out of memory\n", __FUNCTION__, __LINE__));
    return;
  }

  mNextReceiveIndex = TOKEN_NEXT (mNextReceiveIndex);

  //
  // Status for ReceiveToken->CompletionToken.Status.
  //
  if (!EFI_ERROR (Status)) {
    NewEntry->Buffer
      = ReceiveToken->Packet.RxData->FragmentTable[0].FragmentBuffer;
    NewEntry->BufferSize
      = ReceiveToken->Packet.RxData->FragmentTable[0].FragmentLength;
    //
    // Prepare to receive more data.
    //
    SubmitRecieveToken ();
  } else {
    // Fatal receive error. Put an entry with NULL in the queue, signifying
    // to return EFI_DEVICE_ERROR from TcpTransportReceive.
    NewEntry->Buffer     = NULL;
    NewEntry->BufferSize = 0;

    DEBUG ((EFI_D_ERROR, "TCP receive error - %r\n", Status));
  }

  InsertTailList (&mPacketListHead, &NewEntry->Link);

  Status = gBS->SignalEvent (mReceiveEvent);
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "SignalEvent failed - %r\n", Status));
  }

  return;
}

/*
  Event notify function to be called when we accept an incoming TCP connection.

  @param[in]  Event                 The event.
  @param[in]  Context               The context of event.

  @retval  NA

*/
STATIC
VOID
EFIAPI
ConnectionAccepted (
  IN  EFI_EVENT                     Event,
  IN  VOID                          *Context
  )
{
  EFI_STATUS             Status = EFI_ABORTED;
  EFI_TCP4_LISTEN_TOKEN  *AcceptToken = NULL;
  UINTN                  Index = 0;

  AcceptToken = (EFI_TCP4_LISTEN_TOKEN *)Context;
  Status      = AcceptToken->CompletionToken.Status;
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "TCP connection failed - %r\n", Status));
    return;
  }

  DEBUG ((EFI_D_ERROR, "TCP connection received.\n"));

  //
  // Accepting a new TCP connection creates a new instance of the TCP protocol.
  // Open it and prepare to receive on it.
  //
  Status = gBS->OpenProtocol (
                  AcceptToken->NewChildHandle,
                  &gEfiTcp4ProtocolGuid,
                  (VOID **)&mTcpConnection,
                  gImageHandle,
                  NULL,
                  EFI_OPEN_PROTOCOL_GET_PROTOCOL
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "Open TCP connection failed - %r\n", Status));
    return;
  }

  mNextSubmitIndex  = 0;
  mNextReceiveIndex = 0;

  for (Index = 0; Index < NUM_RX_TOKENS; Index++) {
    Status = gBS->CreateEvent (
                    EVT_NOTIFY_SIGNAL,
                    TPL_CALLBACK,
                    DataReceived,
                    NULL,
                    &(mReceiveToken[Index].CompletionToken.Event)
                    );
    if (EFI_ERROR (Status)) {
      DEBUG ((EFI_D_ERROR, "CreateEvent failed - %r\n", Status));
    }
  }

  for (Index = 0; Index < NUM_RX_TOKENS; Index++) {
    SubmitRecieveToken ();
  }
}

/*
  Set up the transport system for use by TCP.
  e.g. For USB this probably means making the device enumerable. For TCP,
       preparing to accept incoming connections.

  If there is a fatal error in the receive channel, ReceiveEvent will be
  signalled, and a subsequent call to Receive() will return an error. This
  allows data transported prior to the error to be received.

  @param[in]  ReceiveEvent          Event to be Signalled when a packet has been received
                                    and is ready to be retrieved via Receive().

  @retval  EFI_SUCCESS              Initialised successfully.
  @retval  EFI_DEVICE_ERROR         Error in initialising hardware
  @retval  Others                   Error return from LocateProtocol functions.

*/
EFI_STATUS
EFIAPI
TcpTransportStart (
  IN  EFI_EVENT                     ReceiveEvent
  )
{
  EFI_STATUS            Status = EFI_ABORTED;
  EFI_HANDLE            NetDeviceHandle = NULL;
  EFI_HANDLE            *HandleBuffer = NULL;
  EFI_IP4_MODE_DATA     Ip4ModeData;
  UINTN                 NumHandles = 0;
  CHAR16                IpAddrString[16];
  UINTN                 Index = 0;
  EFI_TCP4_CONFIG_DATA  TcpConfigData = {
    0x00,                                           // IPv4 Type of Service
    255,                                            // IPv4 Time to Live
    {                                               // AccessPoint:
      TRUE,                                         // Use default address
      {
        { 192, 168, 3, 20 }
      },                                            // IP Address  (ignored - use default)
      {
        { 255, 255, 255, 0 }
      },                                            // Subnet mask (ignored - use default)
      FixedPcdGet32 (PcdTcpPort),                   // Station port
      {
        { 0, 0, 0, 0 }
      },                                            // Remote address: accept any
      0,                                            // Remote Port: accept any
      FALSE                                         // ActiveFlag: be a "server"
    },
    NULL                                            // Default advanced TCP options
  };

  ZeroMem (&Ip4ModeData, sizeof (EFI_IP4_MODE_DATA));

  mReceiveEvent = ReceiveEvent;
  InitializeListHead (&mPacketListHead);

  mTextOut->OutputString (mTextOut, L"Initialising TCP transport...\r\n");

  //
  // Open a passive TCP instance.
  //
  Status = gBS->LocateHandleBuffer (
                  ByProtocol,
                  &gEfiTcp4ServiceBindingProtocolGuid,
                  NULL,
                  &NumHandles,
                  &HandleBuffer
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "Locate EfiTcp4ServiceBindingProtocol failed - %r\n", Status));
    return Status;
  }

  //
  // We just use the first network device.
  //
  NetDeviceHandle = HandleBuffer[0];

  Status = gBS->OpenProtocol (
                  NetDeviceHandle,
                  &gEfiTcp4ServiceBindingProtocolGuid,
                  (VOID **)&mTcpServiceBinding,
                  gImageHandle,
                  NULL,
                  EFI_OPEN_PROTOCOL_GET_PROTOCOL
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "Open EfiTcp4ServiceBindingProtocol failed - %r\n", Status));
    return Status;
  }

  Status = mTcpServiceBinding->CreateChild (mTcpServiceBinding, &mTcpHandle);
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "CreateChild failed - %r\n", Status));
    return Status;
  }

  Status = gBS->OpenProtocol (
                  mTcpHandle,
                  &gEfiTcp4ProtocolGuid,
                  (VOID **)&mTcpListener,
                  gImageHandle,
                  NULL,
                  EFI_OPEN_PROTOCOL_GET_PROTOCOL
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "Open EfiTcp4Protocol failed - %r\n", Status));
    return Status;
  }

  //
  // Set up re-usable tokens.
  //
  for (Index = 0; Index < NUM_RX_TOKENS; Index++) {
    mRxData[Index].UrgentFlag          = FALSE;
    mRxData[Index].FragmentCount       = 1;
    mReceiveToken[Index].Packet.RxData = &mRxData[Index];
  }

  mTxData.Push                 = TRUE;
  mTxData.Urgent               = FALSE;
  mTxData.FragmentCount        = 1;
  mTransmitToken.Packet.TxData = &mTxData;

  Status = gBS->CreateEvent (
                  EVT_NOTIFY_SIGNAL,
                  TPL_CALLBACK,
                  ConnectionAccepted,
                  &mAcceptToken,
                  &mAcceptToken.CompletionToken.Event
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "CreateEvent failed - %r\n", Status));
    return Status;
  }

  Status = gBS->CreateEvent (
                  EVT_NOTIFY_SIGNAL,
                  TPL_CALLBACK,
                  ConnectionClosed,
                  &mCloseToken,
                  &mCloseToken.CompletionToken.Event
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "CreateEvent failed - %r\n", Status));
    return Status;
  }

  //
  // Configure the TCP instance.
  //
  Status = mTcpListener->Configure (mTcpListener, &TcpConfigData);
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "TCP configure failed - %r\n", Status));
    return Status;
  }

  Status = mTcpListener->GetModeData (
                          mTcpListener,
                          NULL,
                          NULL,
                          &Ip4ModeData,
                          NULL,
                          NULL
                          );
  if (!EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "IP: %d.%d.%d.%d\n",
              Ip4ModeData.ConfigData.StationAddress.Addr[0],
              Ip4ModeData.ConfigData.StationAddress.Addr[1],
              Ip4ModeData.ConfigData.StationAddress.Addr[2],
              Ip4ModeData.ConfigData.StationAddress.Addr[3]
              ));
  }

  //
  // Tell the user our address and hostname.
  //
  IP4_ADDR_TO_STRING (Ip4ModeData.ConfigData.StationAddress, IpAddrString);

  mTextOut->OutputString (mTextOut, L"TCP transport configured.");
  mTextOut->OutputString (mTextOut, L"\r\nIP address: ");
  mTextOut->OutputString (mTextOut, IpAddrString);
  mTextOut->OutputString (mTextOut, L"\r\n");

  //
  // Start listening for a connection.
  //
  Status = mTcpListener->Accept (mTcpListener, &mAcceptToken);
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "TCP accept failed - %r\n", Status));
    return Status;
  }

  mTextOut->OutputString (mTextOut, L"TCP transport initialized.\r\n");

  FreePool (HandleBuffer);

  return EFI_SUCCESS;
}

/*
  Function to be called when all TCP transactions are finished, to
  de-initialise the transport system.
  e.g. A USB OTG system might want to get out of peripheral mode so it can be
       a USB host.

  Note that this function will be called after an error is reported by Send or
  Receive

  @param  NA

  @retval  EFI_SUCCESS              De-initialized successfully.
  @retval  EFI_DEVICE_ERROR         Error de-initialising hardware.

*/
EFI_STATUS
EFIAPI
TcpTransportStop (
  VOID
  )
{
  EFI_STATUS            Status = EFI_ABORTED;
  EFI_TCP4_CLOSE_TOKEN  CloseToken;
  UINTN                 EventIndex = 0;
  TCP_PACKET_LIST       *Entry = NULL;
  TCP_PACKET_LIST       *NextEntry = NULL;

  //
  // Close any existing TCP connection, blocking until it's done.
  //
  if (mTcpConnection != NULL) {
    CloseReceiveEvents ();

    CloseToken.AbortOnClose = FALSE;

    Status = gBS->CreateEvent (0, 0, NULL, NULL, &CloseToken.CompletionToken.Event);
    if (EFI_ERROR (Status)) {
      DEBUG ((EFI_D_ERROR, "CreateEvent failed - %r\n", Status));
      return Status;
    }


    Status = mTcpConnection->Close (mTcpConnection, &CloseToken);
    if (EFI_ERROR (Status)) {
      DEBUG ((EFI_D_ERROR, "TCP close failed - %r\n", Status));
      return Status;
    }

    Status = gBS->WaitForEvent (
                    1,
                    &CloseToken.CompletionToken.Event,
                    &EventIndex
                    );
    if (EFI_ERROR (Status)) {
      DEBUG ((EFI_D_ERROR, "WaitForEvent failed - %r\n", Status));
      return Status;
    }

    Status = CloseToken.CompletionToken.Status;
    if (EFI_ERROR (Status)) {
      DEBUG ((EFI_D_ERROR, "TCP complete failed - %r\n", Status));
      return Status;
    }

    //
    // Possible bug in EDK2 TCP4 driver: closing a connection doesn't remove its
    // PCB from the list of live connections. Subsequent attempts to Configure()
    // a TCP instance with the same local port will fail with INVALID_PARAMETER.
    // Calling Configure with NULL is a workaround for this issue.
    //
    (VOID)(mTcpConnection->Configure (mTcpConnection, NULL));
  }

  gBS->CloseEvent (mAcceptToken.CompletionToken.Event);

  //
  // Stop listening for connections.
  // Ideally we would do this with Cancel, but it isn't implemented by EDK2.
  // So we just "reset this TCPv4 instance brutally".
  //
  (VOID)(mTcpListener->Configure (mTcpListener, NULL));

  (VOID)(mTcpServiceBinding->DestroyChild (mTcpServiceBinding, mTcpHandle));

  //
  // Free any data the user didn't pick up.
  //
  Entry = (TCP_PACKET_LIST *)(GetFirstNode (&mPacketListHead));
  while (!IsNull (&mPacketListHead, &Entry->Link)) {
    NextEntry = (TCP_PACKET_LIST *)GetNextNode (&mPacketListHead, &Entry->Link);

    RemoveEntryList (&Entry->Link);
    if (Entry->Buffer) {
      FreePool (Entry->Buffer);
    }

    FreePool (Entry);

    Entry = NextEntry;
  }

  return EFI_SUCCESS;
}

/*
  Event notify function for when data has been sent. Free resources and report
  errors.
  Context should point to the transmit IO token passed to TcpConnection->Transmit.

  @param[in]  Event                 The event.
  @param[in]  Context               The context of event.

  @retval  NA

*/
STATIC
VOID
DataSent (
  IN  EFI_EVENT                     Event,
  IN  VOID                          *Context
  )
{
  EFI_STATUS  Status = EFI_ABORTED;

  Status = mTransmitToken.CompletionToken.Status;
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "TCP transmit result - %r\n", Status));
    gBS->SignalEvent (*(EFI_EVENT *)Context);
  }

  FreePool (mTransmitToken.Packet.TxData->FragmentTable[0].FragmentBuffer);
}

/*
  Send data. This function can be used both for command responses like "OKAY"
  and for the data phase (the protocol doesn't describe any situation when the
  latter might be necessary, but does allow it)

  Transmission need not finish before the function returns.
  If there is an error in transmission from which the transport system cannot
  recover, FatalErrorEvent will be signalled. Otherwise, it is assumed that all
  data was delivered successfully.

  @param[in]  BufferSize            Size in bytes of data to send.
  @param[in]  Buffer                Data to send.
  @param[in]  FatalErrorEvent       Event to signal if there was an error in
                                    transmission from which the transport system
                                    cannot recover.

  @retval  EFI_SUCCESS              The data was sent or queued for send.
  @retval  EFI_DEVICE_ERROR         There was an error preparing to send the data.

 */
EFI_STATUS
EFIAPI
TcpTransportSend (
  IN  UINTN                         BufferSize,
  IN  CONST VOID                    *Buffer,
  IN  EFI_EVENT                     *FatalErrorEvent
  )
{
  EFI_STATUS  Status = EFI_ABORTED;

  if (BufferSize > MAX_BUFFER_SIZE) {
    return EFI_INVALID_PARAMETER;
  }

  //
  // Build transmit IO token.
  //
  // Create an event so we are notified when a transmission is complete.
  // We use this to free resources and report errors.
  //
  Status = gBS->CreateEvent (
                  EVT_NOTIFY_SIGNAL,
                  TPL_CALLBACK,
                  DataSent,
                  FatalErrorEvent,
                  &mTransmitToken.CompletionToken.Event
                  );
  if (EFI_ERROR (Status)) {
    return Status;
  }

  mTxData.DataLength = (UINT32)BufferSize;

  mTxData.FragmentTable[0].FragmentLength = (UINT32)BufferSize;
  mTxData.FragmentTable[0].FragmentBuffer = AllocateCopyPool (
                                              BufferSize,
                                              Buffer
                                              );

  Status = mTcpConnection->Transmit (mTcpConnection, &mTransmitToken);
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "TCP Transmit failed - %r\n", Status));
    return Status;
  }

  return EFI_SUCCESS;
}

/*
  When the event has been Signalled to say data is available from the host,
  this function is used to get data. In order to handle the case where several
  packets are received before ReceiveEvent's notify function is called, packets
  received are queued, and each call to this function returns the next packet in
  the queue. It should therefore be called in a loop, the exit condition being a
  return of EFI_NOT_READY.

  @param[out]  Buffer               Pointer to received data. Callee allocated - the
                                    caller must free it with FreePool.
  @param[out]  BufferSize           The size of received data in bytes

  @retval  EFI_NOT_READY            There is no data available
  @retval  EFI_DEVICE_ERROR         There was a fatal error in the receive channel.
                                    e.g. for USB the cable was unplugged or for TCP the
                                    connection was closed by the remote host..

*/
EFI_STATUS
EFIAPI
TcpTransportReceive (
  OUT UINTN                         *BufferSize,
  OUT VOID                          **Buffer
  )
{
  TCP_PACKET_LIST  *Entry = NULL;

  if (IsListEmpty (&mPacketListHead)) {
    return EFI_NOT_READY;
  }

  Entry = (TCP_PACKET_LIST *)(GetFirstNode (&mPacketListHead));

  if (NULL == Entry->Buffer) {
    //
    // There was an error receiving this packet.
    //
    return EFI_DEVICE_ERROR;
  }

  *Buffer     = Entry->Buffer;
  *BufferSize = Entry->BufferSize;

  RemoveEntryList (&Entry->Link);
  FreePool (Entry);

  return EFI_SUCCESS;
}

TCP_TRANSPORT_PROTOCOL  mTransportProtocol = {
  TCP_TRANSPORT_PROTOCOL_REVISION_1_0,
  TcpTransportStart,
  TcpTransportStop,
  TcpTransportSend,
  TcpTransportReceive
};

/**
  Install TCP transport protocol.

  @param[in]  ImageHandle           Image handle this driver.
  @param[in]  SystemTable           Pointer to the System Table.

  @retval  EFI_SUCCESS              Driver executed successfully.
  @retval  Others                   Error happened.

**/
EFI_STATUS
TcpTransportDxeEntryPoint (
  IN  EFI_HANDLE                    ImageHandle,
  IN  EFI_SYSTEM_TABLE              *SystemTable
  )
{
  EFI_STATUS  Status = EFI_ABORTED;

  Status = gBS->LocateProtocol (
                  &gEfiSimpleTextOutProtocolGuid,
                  NULL,
                  (VOID **)&mTextOut
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "Locate EfiSimpleTextOutProtocol failed - %r\n", Status));
    return Status;
  }

  Status = gBS->InstallProtocolInterface (
                  &ImageHandle,
                  &gTcpTransportProtocolGuid,
                  EFI_NATIVE_INTERFACE,
                  &mTransportProtocol
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "Install TcpTransportProtocol failed - %r\n", Status));
  }

  return Status;
}
