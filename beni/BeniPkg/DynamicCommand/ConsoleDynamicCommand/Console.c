/**
*  @Package     : BeniPkg
*  @FileName    : Console.c
*  @Date        : 20241208
*  @Author      : Jiangwei
*  @Version     : 1.0
*  @Description :
*    This command is for test console-related protocols.
*
*  @History:
*    20241208: Initialize.
*
*  This program and the accompanying materials
*  are licensed and made available under the terms and conditions of the BSD License
*  which accompanies this distribution. The full text of the license may be found at
*  http://opensource.org/licenses/bsd-license.php
*
*  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
*  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
**/

#include "Console.h"

STATIC CONST SHELL_PARAM_ITEM ParamList[] = {
  {L"dev",  TypeFlag},
  {L"in",   TypeFlag},
  {L"in2",  TypeFlag},
  {NULL,    TypeMax }
};

BOOLEAN mBreak = FALSE;

/**
  Notification function for keystrokes.

  @param[in]  KeyData               The key that was pressed.

  @retval   EFI_SUCCESS             The operation was successful.

**/
EFI_STATUS
EFIAPI
NotificationFunction (
  IN  EFI_KEY_DATA                  *KeyData
  )
{
  mBreak = TRUE;

  return EFI_SUCCESS;
}

/**
  Get key and print it.

  @param  NA

  @retval  NA

**/
VOID
WaitforKeyEx (
  VOID
  )
{
  EFI_STATUS                        Status = EFI_ABORTED;
  EFI_SIMPLE_TEXT_INPUT_EX_PROTOCOL *SimpleEx = NULL;
  EFI_KEY_DATA                      KeyData;
  VOID                              *NotifyHandle = NULL;
  UINTN                             EventIndex = 0;

  ZeroMem (&KeyData, sizeof (EFI_KEY_DATA));

  Status = gBS->OpenProtocol (
                  gST->ConsoleInHandle,
                  &gEfiSimpleTextInputExProtocolGuid,
                  (VOID **)&SimpleEx,
                  gImageHandle,
                  NULL,
                  EFI_OPEN_PROTOCOL_GET_PROTOCOL
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Failed. - %r\n", __FUNCTION__, __LINE__, Status));
    return;
  }

  //
  // Signal event when control + c is pressed.
  //
  KeyData.KeyState.KeyToggleState = 0;
  KeyData.Key.ScanCode            = 0;
  KeyData.KeyState.KeyShiftState  = EFI_SHIFT_STATE_VALID | EFI_LEFT_CONTROL_PRESSED;
  KeyData.Key.UnicodeChar         = L'c';

  Status = SimpleEx->RegisterKeyNotify (
                      SimpleEx,
                      &KeyData,
                      NotificationFunction,
                      &NotifyHandle
                      );
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Failed. - %r\n", __FUNCTION__, __LINE__, Status));
    return;
  }

  Print (L"Input something:\r\n");
  while (TRUE) {
    gBS->WaitForEvent (1, &SimpleEx->WaitForKeyEx, &EventIndex);
    if (mBreak) {
      break;
    }
    Status = SimpleEx->ReadKeyStrokeEx (SimpleEx, &KeyData);
    if (!EFI_ERROR (Status)) {
      Print (L"The input character: %c\r\n", KeyData.Key.UnicodeChar);
    }
  }

  return;
}

/**
  Get key and print it.

  @param  NA

  @retval  NA

**/
VOID
WaitforKey (
  VOID
  )
{
  EFI_STATUS    Status = EFI_ABORTED;
  UINTN         EventIndex = 0;
  EFI_INPUT_KEY Key = {0, 0};

  Print (L"Input something:\r\n");
  while (TRUE) {
    gBS->WaitForEvent (1, &gST->ConIn->WaitForKey, &EventIndex);
    Status = gST->ConIn->ReadKeyStroke (gST->ConIn, &Key);
    if (!EFI_ERROR (Status)) {
      if ((SCAN_ESC == Key.ScanCode)) {
        Print (L"Input end.\r\n");
        break;
      }
      Print (L"The input character: %c\r\n", Key.UnicodeChar);
    }
  }
}

/**
  Show device path of consoles.

  @param  NA

  @retval  NA

**/
VOID
ShowDev (
  VOID
  )
{
  EFI_STATUS                        Status = EFI_ABORTED;
  EFI_HANDLE                        *Handles = NULL;
  UINTN                             HandleCount = 0;
  UINTN                             Index = 0;
  EFI_SIMPLE_TEXT_INPUT_PROTOCOL    *SimpleInput = NULL;
  EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL   *SimpleTextOutput = NULL;
  EFI_SIMPLE_TEXT_INPUT_EX_PROTOCOL *SimpleInputEx = NULL;
  EFI_DEVICE_PATH_PROTOCOL          *DevPath = NULL;

  Status = gBS->LocateHandleBuffer (
                  ByProtocol,
                  &gEfiSimpleTextInProtocolGuid,
                  NULL,
                  &HandleCount,
                  &Handles
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Failed. - %r\n", __FUNCTION__, __LINE__, Status));
  } else {
    for (Index = 0; Index < HandleCount; Index++) {
      Status = gBS->HandleProtocol (
                      Handles[Index],
                      &gEfiSimpleTextInProtocolGuid,
                      (VOID **)&SimpleInput
                      );
      if (!EFI_ERROR (Status)) {
        DevPath = DevicePathFromHandle (Handles[Index]);
        if (NULL != DevPath) {
          Print (L"ConIn%d: %s\r\n", Index, ConvertDevicePathToText (DevPath, TRUE, FALSE));
        }
      }
    }
  }

  Print (L"\r\n");
  Status = gBS->LocateHandleBuffer (
                  ByProtocol,
                  &gEfiSimpleTextInputExProtocolGuid,
                  NULL,
                  &HandleCount,
                  &Handles
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Failed. - %r\n", __FUNCTION__, __LINE__, Status));
  } else {
    for (Index = 0; Index < HandleCount; Index++) {
      Status = gBS->HandleProtocol (
                      Handles[Index],
                      &gEfiSimpleTextInputExProtocolGuid,
                      (VOID **)&SimpleInputEx
                      );
      if (!EFI_ERROR (Status)) {
        DevPath = DevicePathFromHandle (Handles[Index]);
        if (NULL != DevPath) {
          Print (L"ConInEx%d: %s\r\n", Index, ConvertDevicePathToText (DevPath, TRUE, FALSE));
        }
      }
    }
  }

  Print (L"\r\n");
  Status = gBS->LocateHandleBuffer (
                  ByProtocol,
                  &gEfiSimpleTextOutProtocolGuid,
                  NULL,
                  &HandleCount,
                  &Handles
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Failed. - %r\n", __FUNCTION__, __LINE__, Status));
  } else {
    for (Index = 0; Index < HandleCount; Index++) {
      Status = gBS->HandleProtocol (
                      Handles[Index],
                      &gEfiSimpleTextOutProtocolGuid,
                      (VOID **)&SimpleTextOutput
                      );
      if (!EFI_ERROR (Status)) {
        DevPath = DevicePathFromHandle (Handles[Index]);
        if (NULL != DevPath) {
          Print (L"ConOut%d: %s\r\n", Index, ConvertDevicePathToText (DevPath, TRUE, FALSE));
          Print (L"  MaxMode       : %d\r\n", SimpleTextOutput->Mode->MaxMode);
          Print (L"  Mode          : %d\r\n", SimpleTextOutput->Mode->Mode);
          Print (L"  Attribute     : 0x%x\r\n", SimpleTextOutput->Mode->Attribute);
          Print (L"  CursorColumn  : %d\r\n", SimpleTextOutput->Mode->CursorColumn);
          Print (L"  CursorRow     : %d\r\n", SimpleTextOutput->Mode->CursorRow);
          Print (L"  CursorVisible : %d\r\n", SimpleTextOutput->Mode->CursorVisible);
        }
      }
    }
  }

  if (NULL != Handles) {
    FreePool (Handles);
  }
}

/**
  Function for 'console' command.

  @param[in]  ImageHandle           The image handle.
  @param[in]  SystemTable           The system table.

  @retval  SHELL_SUCCESS            Command completed successfully.
  @retval  SHELL_INVALID_PARAMETER  Command usage error.
  @retval  SHELL_ABORTED            The user aborts the operation.
  @retval  Value                    Unknown error.

**/
SHELL_STATUS
RunConsole (
  IN  EFI_HANDLE                    ImageHandle,
  IN  EFI_SYSTEM_TABLE              *SystemTable
  )
{
  SHELL_STATUS                    ShellStatus = SHELL_INVALID_PARAMETER;
  EFI_STATUS                      Status = EFI_ABORTED;
  LIST_ENTRY                      *CheckPackage = NULL;
  CHAR16                          *ProblemParam = NULL;
  UINTN                           ParamCount = 0;

  //
  // Initialize the Shell library (we must be in non-auto-init...).
  //
  Status = ShellInitialize ();
  if (EFI_ERROR (Status)) {
    return SHELL_ABORTED;
  }

  //
  // Parse the command line.
  //
  Status = ShellCommandLineParse (ParamList, &CheckPackage, &ProblemParam, TRUE);
  if (EFI_ERROR (Status)) {
    if ((Status == EFI_VOLUME_CORRUPTED) && (ProblemParam != NULL) ) {
      ShellPrintHiiEx (
        -1, -1, NULL, STRING_TOKEN (STR_GEN_PROBLEM),
        mConsoleHiiHandle, L"console", ProblemParam
        );
      FreePool (ProblemParam);
    }
    goto DONE;
  }

  //
  // Check the number of parameters
  //
  ParamCount = ShellCommandLineGetCount (CheckPackage);
  if (ParamCount > 2) {
    ShellPrintHiiEx (
      -1, -1, NULL, STRING_TOKEN (STR_GEN_TOO_MANY),
      mConsoleHiiHandle, L"console"
      );
    goto DONE;
  }

  if (ShellCommandLineGetFlag (CheckPackage, L"dev")) {
    ShowDev ();
  } else if (ShellCommandLineGetFlag (CheckPackage, L"in")) {
    WaitforKey ();
  } else if (ShellCommandLineGetFlag (CheckPackage, L"in2")) {
    WaitforKeyEx ();
  }

DONE:

  if ((ShellStatus != SHELL_SUCCESS) && (EFI_ERROR (Status))) {
    ShellStatus = Status & ~MAX_BIT;
  }

  return ShellStatus;
}
