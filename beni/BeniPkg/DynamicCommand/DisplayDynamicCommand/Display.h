/**
*  @Package     : BeniPkg
*  @FileName    : Display.h
*  @Date        : 20230623
*  @Author      : Jiangwei
*  @Version     : 1.0
*  @Description :
*    This command is for test display protocol.
*
*  @History:
*    20230623: Initialize.
*
*  This program and the accompanying materials
*  are licensed and made available under the terms and conditions of the BSD License
*  which accompanies this distribution. The full text of the license may be found at
*  http://opensource.org/licenses/bsd-license.php
*
*  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
*  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
**/

#ifndef __DISPLAY_H__
#define __DISPLAY_H__

#include <Uefi.h>

#include <IndustryStandard/Pci.h>

#include <Library/UefiLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/HiiLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/ShellLib.h>
#include <Library/DevicePathLib.h>
#include <Library/BeniHiiLib.h>

#include <Protocol/HiiPackageList.h>
#include <Protocol/ShellDynamicCommand.h>
#include <Protocol/DevicePath.h>
#include <Protocol/GraphicsOutput.h>
#include <Protocol/SimpleTextOut.h>
#include <Protocol/HiiFont.h>
#include <Protocol/HiiDatabase.h>

#define FILL_STEP     10
#define BAR_HEIGHT    20
#define NARROW_WIDTH  8
#define NARROW_HEIGHT 19

/**
  Function for 'display' command.

  @param[in]  ImageHandle           The image handle.
  @param[in]  SystemTable           The system table.

  @retval  SHELL_SUCCESS            Command completed successfully.
  @retval  SHELL_INVALID_PARAMETER  Command usage error.
  @retval  SHELL_ABORTED            The user aborts the operation.
  @retval  Value                    Unknown error.

**/
SHELL_STATUS
RunDisplay (
  IN  EFI_HANDLE                    ImageHandle,
  IN  EFI_SYSTEM_TABLE              *SystemTable
  );

/**
  Show special font.

  @param[in]  Gop                   Pointer to EFI_GRAPHICS_OUTPUT_PROTOCOL.

  @retval  NA

**/
VOID
ShowFont (
  IN  EFI_GRAPHICS_OUTPUT_PROTOCOL  *Gop
  );

//
// Used for shell display.
//
extern EFI_HII_HANDLE mDisplayHiiHandle;

#endif // __DISPLAY_H__
