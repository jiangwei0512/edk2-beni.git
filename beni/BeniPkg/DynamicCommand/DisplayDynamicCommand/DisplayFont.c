/**
*  @Package     : BeniPkg
*  @FileName    : Display.c
*  @Date        : 20230623
*  @Author      : Jiangwei
*  @Version     : 1.0
*  @Description :
*    This command is for test display protocol.
*
*  @History:
*    20230623: Initialize.
*
*  This program and the accompanying materials
*  are licensed and made available under the terms and conditions of the BSD License
*  which accompanies this distribution. The full text of the license may be found at
*  http://opensource.org/licenses/bsd-license.php
*
*  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
*  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
**/

#include "Display.h"

#define NARROW_GLYPH_NUMBER 8
#define WIDE_GLYPH_NUMBER   75
#define NARROW_GLYPH_LEN    9

//
// {2E7BAEE7-6BBD-4955-AC79-176EA6D5A62D}
// This should be different with the one in MdeModulePkg\Application\UiApp\String.c.
//
EFI_GUID  mFontPackageGuid = {
  0x2e7baee7, 0x6bbd, 0x4955, { 0xac, 0x79, 0x17, 0x6e, 0xa6, 0xd5, 0xa6, 0x2d }
};

typedef struct {
  ///
  /// This 4-bytes total array length is required by HiiAddPackages()
  ///
  UINT32                    Length;

  //
  // This is the Font package definition
  //
  EFI_HII_PACKAGE_HEADER    Header;
  UINT16                    NumberOfNarrowGlyphs;
  UINT16                    NumberOfWideGlyphs;
  EFI_NARROW_GLYPH          NarrowArray[NARROW_GLYPH_NUMBER];
  EFI_WIDE_GLYPH            WideArray[WIDE_GLYPH_NUMBER];
} FONT_PACK_BIN;

FONT_PACK_BIN  mFontBin = {
  sizeof (FONT_PACK_BIN),
  {
    sizeof (FONT_PACK_BIN) - sizeof (UINT32),
    EFI_HII_PACKAGE_SIMPLE_FONTS,
  },
  NARROW_GLYPH_NUMBER,
  0,
  {     // Narrow Glyphs
    {
      0x05d0,
      0x00,
      {
        0x00,
        0x00,
        0x00,
        0x4E,
        0x6E,
        0x62,
        0x32,
        0x32,
        0x3C,
        0x68,
        0x4C,
        0x4C,
        0x46,
        0x76,
        0x72,
        0x00,
        0x00,
        0x00,
        0x00
      }
    },
    {
      0x05d1,
      0x00,
      {
        0x00,
        0x00,
        0x00,
        0x78,
        0x7C,
        0x0C,
        0x0C,
        0x0C,
        0x0C,
        0x0C,
        0x0C,
        0x0C,
        0x0C,
        0x7E,
        0x7E,
        0x00,
        0x00,
        0x00,
        0x00
      }
    },
    {
      0x05d2,
      0x00,
      {
        0x00,
        0x00,
        0x00,
        0x78,
        0x7C,
        0x0C,
        0x0C,
        0x0C,
        0x0C,
        0x0C,
        0x0C,
        0x1C,
        0x3E,
        0x66,
        0x66,
        0x00,
        0x00,
        0x00,
        0x00
      }
    },
    {
      0x05d3,
      0x00,
      {
        0x00,
        0x00,
        0x00,
        0x7E,
        0x7E,
        0x0C,
        0x0C,
        0x0C,
        0x0C,
        0x0C,
        0x0C,
        0x0C,
        0x0C,
        0x0C,
        0x0C,
        0x00,
        0x00,
        0x00,
        0x00
      }
    },
    {
      0x05d4,
      0x00,
      {
        0x00,
        0x00,
        0x00,
        0x7C,
        0x7E,
        0x06,
        0x06,
        0x06,
        0x06,
        0x66,
        0x66,
        0x66,
        0x66,
        0x66,
        0x66,
        0x00,
        0x00,
        0x00,
        0x00
      }
    },
    {
      0x05d5,
      0x00,
      {
        0x00,
        0x00,
        0x00,
        0x3C,
        0x3C,
        0x0C,
        0x0C,
        0x0C,
        0x0C,
        0x0C,
        0x0C,
        0x0C,
        0x0C,
        0x0C,
        0x0C,
        0x00,
        0x00,
        0x00,
        0x00
      }
    },
    {
      0x05d6,
      0x00,
      {
        0x00,
        0x00,
        0x00,
        0x38,
        0x38,
        0x1E,
        0x1E,
        0x18,
        0x18,
        0x18,
        0x18,
        0x18,
        0x18,
        0x18,
        0x18,
        0x00,
        0x00,
        0x00,
        0x00
      }
    },
    {
      0x0000,
      0x00,
      {
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00
      }
    }
  }
};

/**
  Routine to export glyphs to the HII database.
  This is in addition to whatever is defined in the Graphics Console driver.

**/
EFI_HII_HANDLE
ExportFonts (
  VOID
  )
{
  return HiiAddPackages (
           &mFontPackageGuid,
           gImageHandle,
           &mFontBin,
           NULL
           );
}

/**
  Show special font.

  @param[in]  Gop                   Pointer to EFI_GRAPHICS_OUTPUT_PROTOCOL.

  @retval  NA

**/
VOID
ShowFont (
  IN  EFI_GRAPHICS_OUTPUT_PROTOCOL  *Gop
  )
{
  EFI_STATUS            Status = EFI_ABORTED;
  EFI_HII_HANDLE        HiiHandle = NULL;
  EFI_HII_FONT_PROTOCOL *HiiFont = NULL;
  EFI_IMAGE_OUTPUT      *Blt = NULL;
  UINTN                 Index = 0;

  //
  // Install customized fonts needed by Front Page
  //
  HiiHandle = ExportFonts ();
  if (NULL == HiiHandle) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Failed.\n", __FUNCTION__, __LINE__));
    return;
  }

  Status = gBS->LocateProtocol (
                  &gEfiHiiFontProtocolGuid,
                  NULL,
                  (VOID **)&HiiFont
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Failed. - %r\n", __FUNCTION__, __LINE__, Status));
    goto DONE;
  }

  for (Index = 0; Index < (NARROW_GLYPH_NUMBER - 1); Index++) {
    Status = HiiFont->GetGlyph (
                        HiiFont,
                        (CHAR16)(0x05d0 + Index),
                        NULL,
                        &Blt,
                        NULL
                        );
    if (EFI_ERROR (Status)) {
      DEBUG ((EFI_D_ERROR, "[%a][%d] Failed. - %r\n", __FUNCTION__, __LINE__, Status));
      goto DONE;
    }

    Gop->Blt (
          Gop,
          Blt->Image.Bitmap,
          EfiBltBufferToVideo,
          0,
          0,
          NARROW_WIDTH * Index,
          0,
          Blt->Width,
          Blt->Height,
          0
          );
    if (NULL != Blt) {
      FreePool (Blt);
      Blt = NULL;
    }
  }

DONE:

  if (NULL != HiiHandle) {
    HiiRemovePackages (HiiHandle);
  }
}
