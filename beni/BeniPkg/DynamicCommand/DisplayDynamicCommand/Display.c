/**
*  @Package     : BeniPkg
*  @FileName    : Display.c
*  @Date        : 20230623
*  @Author      : Jiangwei
*  @Version     : 1.0
*  @Description :
*    This command is for test display protocol.
*
*  @History:
*    20230623: Initialize.
*
*  This program and the accompanying materials
*  are licensed and made available under the terms and conditions of the BSD License
*  which accompanies this distribution. The full text of the license may be found at
*  http://opensource.org/licenses/bsd-license.php
*
*  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
*  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
**/

#include "Display.h"

STATIC CONST SHELL_PARAM_ITEM ParamList[] = {
  {L"mode",     TypeFlag},
  {L"blink",    TypeFlag},
  {L"fill",     TypeFlag},
  {L"bar1",     TypeFlag},
  {L"bar2",     TypeFlag},
  {L"a",        TypeFlag},
  {L"b",        TypeFlag},
  {L"textmode", TypeFlag},
  {L"font",     TypeFlag},
  {NULL,        TypeMax }
};

CHAR16 *gPixelFormat[] = {
  L"PixelRedGreenBlueReserved8BitPerColor",
  L"PixelBlueGreenRedReserved8BitPerColor",
  L"PixelBitMask",
  L"PixelBltOnly",
  L"PixelFormatMax",
};

enum _DISPLAY_COLCR_ {
  EnumWhite = 0,
  EnumBlue,
  EnumGreen,
  EnumRed,
  EnumBlack,
  EnumMax
};

EFI_GRAPHICS_OUTPUT_BLT_PIXEL mWhite = {
  255,
  255,
  255,
  0
};

EFI_GRAPHICS_OUTPUT_BLT_PIXEL mBlue = {
  255,
  0,
  0,
  0
};

EFI_GRAPHICS_OUTPUT_BLT_PIXEL mGreen = {
  0,
  255,
  0,
  0
};

EFI_GRAPHICS_OUTPUT_BLT_PIXEL mRed = {
  0,
  0,
  255,
  0
};

EFI_GRAPHICS_OUTPUT_BLT_PIXEL mBlack = {
  0,
  0,
  0,
  0
};

/**
  Get EFI_GRAPHICS_OUTPUT_PROTOCOL representing QEMU video card.

  @param[out] Gop                             Pointer to EFI_GRAPHICS_OUTPUT_PROTOCOL.
  @param[out] Stp                             Pointer to EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL.

  @retval  EFI_STATUS                         Protocol found.
  @retval  Others                             Protocol not found.

**/
EFI_STATUS
GetSpecificGop (
  OUT EFI_GRAPHICS_OUTPUT_PROTOCOL            **Gop,
  OUT EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL         **Stp
  )
{
  EFI_STATUS                    Status = EFI_NOT_FOUND;
  UINTN                         Index = 0;
  UINTN                         GopHandleCount = 0;
  EFI_HANDLE                    *GopHandleBuffer = NULL;
  EFI_DEVICE_PATH_PROTOCOL      *GopDevicePath = NULL;

  if ((NULL == Gop) || (NULL == Stp)) {
    return EFI_INVALID_PARAMETER;
  }

  //
  // Get all GOP responding independent video card.
  //
  Status = gBS->LocateHandleBuffer (
                  ByProtocol,
                  &gEfiGraphicsOutputProtocolGuid,
                  NULL,
                  &GopHandleCount,
                  &GopHandleBuffer
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Failed. - %r\n", __FUNCTION__, __LINE__, Status));
    goto DONE;
  }

  for (Index = 0; Index < GopHandleCount; Index++) {
    //
    // The video card should have device path.
    //
    Status = gBS->HandleProtocol (
                    GopHandleBuffer[Index],
                    &gEfiDevicePathProtocolGuid,
                    (VOID *)&GopDevicePath
                    );
    if (EFI_ERROR (Status)) {
      continue;
    }
    Status = gBS->HandleProtocol (
                    GopHandleBuffer[Index],
                    &gEfiGraphicsOutputProtocolGuid,
                    Gop
                    );
    if (EFI_ERROR (Status)) {
      continue;
    } else {
      Status = gBS->HandleProtocol (
                      GopHandleBuffer[Index],
                      &gEfiSimpleTextOutProtocolGuid,
                      Stp
                      );
      if (EFI_ERROR (Status)) {
        continue;
      } else {
        Print (L"Video card evice path: %s\r\n",
                ConvertDevicePathToText (GopDevicePath, TRUE, TRUE));
        break;
      }
    }
  }

DONE:

  if (NULL != GopHandleBuffer) {
    FreePool (GopHandleBuffer);
    GopHandleBuffer = NULL;
  }

  return Status;
}

/**
  Print GOP mode.

  @param[in]  Gop                   Pointer to EFI_GRAPHICS_OUTPUT_PROTOCOL.

  @retval  NA

**/
VOID
ShowGopMode (
  IN  EFI_GRAPHICS_OUTPUT_PROTOCOL  *Gop
  )
{
  EFI_STATUS                            Status = EFI_ABORTED;
  UINT32                                Index = 0;
  UINTN                                 SizeOfInfo = 0;
  EFI_GRAPHICS_OUTPUT_MODE_INFORMATION  *ModeInfo = NULL;

  Print (L"MaxMode          : %d\r\n", Gop->Mode->MaxMode);
  Print (L"Current Mode     : %d\r\n", Gop->Mode->Mode);
  Print (L"FrameBufferBase  : 0x%lx\r\n", Gop->Mode->FrameBufferBase);
  Print (L"FrameBufferSize  : 0x%lx\r\n", Gop->Mode->FrameBufferSize);

  for (Index = 0; Index < Gop->Mode->MaxMode; Index++) {
    Status = Gop->QueryMode (Gop, Index, &SizeOfInfo, &ModeInfo);
    if (!EFI_ERROR (Status)) {
      Print (L"---------------------------------------------------------------\r\n");
      Print (L"Mode                           : %d\r\n", Index);
      Print (L"Info.Version                   : 0x%04x\r\n", ModeInfo->Version);
      Print (L"Info.HorizontalResolution      : %d\r\n", ModeInfo->HorizontalResolution);
      Print (L"Info.VerticalResolution        : %d\r\n", ModeInfo->VerticalResolution);
      Print (L"Info.PixelFormat               : %s\r\n", gPixelFormat[ModeInfo->PixelFormat]);
      Print (L"PixelInformation.RedMask       : 0x%04x\r\n",
              ModeInfo->PixelInformation.RedMask);
      Print (L"PixelInformation.GreenMask     : 0x%04x\r\n",
              ModeInfo->PixelInformation.GreenMask);
      Print (L"PixelInformation.BlueMask      : 0x%04x\r\n",
              ModeInfo->PixelInformation.BlueMask);
      Print (L"PixelInformation.ReservedMask  : 0x%04x\r\n",
              ModeInfo->PixelInformation.ReservedMask);
      Print (L"PixelsPerScanLine              : 0x%04x\r\n", ModeInfo->PixelsPerScanLine);
      Print (L"SizeOfInfo                     : %d\r\n", SizeOfInfo);
      FreePool (ModeInfo);
    }
  }
  Print (L"---------------------------------------------------------------\r\n");
}

/**
  Blink the screen.

  @param[in]  Gop                   Pointer to EFI_GRAPHICS_OUTPUT_PROTOCOL.

  @retval  NA

**/
VOID
ScreenBlink (
  IN  EFI_GRAPHICS_OUTPUT_PROTOCOL  *Gop
  )
{
  UINTN                         Width = 0;
  UINTN                         Height = 0;
  EFI_GRAPHICS_OUTPUT_BLT_PIXEL BlinkColor[] = {
    mWhite,
    mBlue,
    mGreen,
    mRed,
    mBlack
  };
  UINTN                         Index = 0;
  UINTN                         Count = sizeof (BlinkColor) / sizeof (BlinkColor[0]);

  Width = Gop->Mode->Info->HorizontalResolution;
  Height = Gop->Mode->Info->VerticalResolution;

  for (Index = 0; Index < Count; Index++) {
    Gop->Blt (
          Gop,
          &BlinkColor[Index],
          EfiBltVideoFill,
          0,
          0,
          0,
          0,
          Width,
          Height,
          0
          );
    gBS->Stall (1000 * 1000);
  }
}

/**
  Fill the screen.

  @param[in]  Gop                   Pointer to EFI_GRAPHICS_OUTPUT_PROTOCOL.

  @retval  NA

**/
VOID
FillScreen (
  IN  EFI_GRAPHICS_OUTPUT_PROTOCOL  *Gop
  )
{
  EFI_STATUS                    Status = EFI_ABORTED;
  EFI_GRAPHICS_OUTPUT_BLT_PIXEL *Blt = NULL;
  UINTN                         Width = 0;
  UINTN                         Height = 0;
  UINT32                        IndexW = 0;
  UINT32                        IndexH = 0;

  Width = Gop->Mode->Info->HorizontalResolution / FILL_STEP;
  Height = Gop->Mode->Info->VerticalResolution / FILL_STEP;

  Blt = AllocateZeroPool (sizeof (EFI_GRAPHICS_OUTPUT_BLT_PIXEL) * Width * Height);
  if (NULL == Blt) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Out of memory\n", __FUNCTION__, __LINE__));
    return;
  }

  for (IndexW = 0; IndexW < Width; IndexW++) {
    for (IndexH = 0; IndexH < Height; IndexH++) {
      Blt[IndexH * Width + IndexW].Red = 255;
    }
  }

  for (IndexW = 0; IndexW < FILL_STEP; IndexW++) {
    for (IndexH = 0; IndexH < FILL_STEP; IndexH++) {
      Status = Gop->Blt (
                      Gop,
                      Blt,
                      EfiBltBufferToVideo,
                      0,
                      0,
                      Width * IndexW,
                      Height * IndexH,
                      Width,
                      Height,
                      0
                      );
      gBS->Stall (1000 * 10);
    }
  }
}

/**
  Show process bar.

  @param[in]  Gop                   Pointer to EFI_GRAPHICS_OUTPUT_PROTOCOL.

  @retval  NA

**/
VOID
ShowBar1 (
  IN  EFI_GRAPHICS_OUTPUT_PROTOCOL  *Gop
  )
{
  EFI_STATUS                    Status = EFI_ABORTED;
  EFI_GRAPHICS_OUTPUT_BLT_PIXEL *Blt = NULL;
  UINTN                         Width = 0;
  UINTN                         Height = 0;
  UINT32                        IndexW = 0;
  UINT32                        IndexH = 0;

  Width = Gop->Mode->Info->HorizontalResolution;  // The width of bar.
  Height = BAR_HEIGHT;  // The height of bar.

  Blt = AllocateZeroPool (sizeof (EFI_GRAPHICS_OUTPUT_BLT_PIXEL) * Width * Height);
  if (NULL == Blt) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Out of memory\n", __FUNCTION__, __LINE__));
    return;
  }
  //
  // Buffer for a red process bar.
  //
  for (IndexW = 0; IndexW < Width; IndexW++) {
    for (IndexH = 0; IndexH < Height; IndexH++) {
      Blt[IndexH * Width + IndexW].Red = 255;
    }
  }
  for (IndexW = 0; IndexW < Width; IndexW++) {
    Status = Gop->Blt (
                    Gop,
                    Blt,
                    EfiBltBufferToVideo,
                    0,
                    0,
                    IndexW,
                    0,
                    1,
                    Height,
                    sizeof (EFI_GRAPHICS_OUTPUT_BLT_PIXEL) * Width
                    );
    gBS->Stall (1000 * 10);
  }
}

/**
  Show process bar.

  @param[in]  Gop                   Pointer to EFI_GRAPHICS_OUTPUT_PROTOCOL.

  @retval  NA

**/
VOID
ShowBar2 (
  IN  EFI_GRAPHICS_OUTPUT_PROTOCOL  *Gop
  )
{
  EFI_STATUS                    Status = EFI_ABORTED;
  EFI_GRAPHICS_OUTPUT_BLT_PIXEL *Blt = NULL;
  UINTN                         Width = 0;
  UINTN                         Height = 0;
  UINT32                        IndexW = 0;
  UINT32                        IndexH = 0;

  Width = Gop->Mode->Info->HorizontalResolution;  // The width of bar.
  Height = BAR_HEIGHT;  // The height of bar.

  Blt = AllocateZeroPool (sizeof (EFI_GRAPHICS_OUTPUT_BLT_PIXEL) * Height);
  if (NULL == Blt) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Out of memory\n", __FUNCTION__, __LINE__));
    return;
  }

  //
  // One column for a blue process bar.
  //
  for (IndexH = 0; IndexH < Height; IndexH++) {
    Blt[IndexH].Blue = 255;
  }
  for (IndexW = 0; IndexW < Width; IndexW++) {
    Status = Gop->Blt (
                    Gop,
                    Blt,
                    EfiBltBufferToVideo,
                    0,
                    0,
                    IndexW,
                    0,
                    1,
                    Height,
                    0
                    );
    gBS->Stall (1000 * 10);
  }
}

/**
  Print an 'A'.

  @param[in]  Gop                   Pointer to EFI_GRAPHICS_OUTPUT_PROTOCOL.

  @retval  NA

**/
VOID
ShowA (
  IN  EFI_GRAPHICS_OUTPUT_PROTOCOL  *Gop
  )
{
  EFI_GRAPHICS_OUTPUT_BLT_PIXEL *Blt = NULL;
  UINTN                         Width = 0;
  UINTN                         Height = 0;
  UINT32                        Index = 0;
  UINT8                         Temp = 0;
  UINT8                         BltIndex[NARROW_HEIGHT] = {
    0x00,
    0x00,
    0x00,
    0x10,
    0x38,
    0x6C,
    0xC6,
    0xC6,
    0xC6,
    0xFE,
    0xC6,
    0xC6,
    0xC6,
    0xC6,
    0xC6,
    0x00,
    0x00,
    0x00,
    0x00
    };

  Width = NARROW_WIDTH;
  Height = NARROW_HEIGHT;  // The height of bar.

  Blt = AllocateZeroPool (sizeof (EFI_GRAPHICS_OUTPUT_BLT_PIXEL) * Width * Height);
  if (NULL == Blt) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Out of memory\n", __FUNCTION__, __LINE__));
    return;
  }

  for (Index = 0; Index < NARROW_HEIGHT; Index++) {
    Temp = 0;
    while (Temp < NARROW_WIDTH) {
      if (BltIndex[Index] & (1 << Temp)) {
        Blt[Index * NARROW_WIDTH + NARROW_WIDTH - 1 - Temp].Red = 255;
      }
      Temp += 1;
    }
  }

  Gop->Blt (
        Gop,
        Blt,
        EfiBltBufferToVideo,
        0,
        0,
        0,
        0,
        Width,
        Height,
        0
        );
}

/**
  Print an 'B'. For ShowB, we get 'B' from HiiFont->

  @param[in]  HiiFont               Pointer to EFI_HII_FONT_PROTOCOL.
  @param[in]  Gop                   Pointer to EFI_GRAPHICS_OUTPUT_PROTOCOL.

  @retval  NA

**/
VOID
ShowB (
  IN  EFI_HII_FONT_PROTOCOL         *HiiFont,
  IN  EFI_GRAPHICS_OUTPUT_PROTOCOL  *Gop
  )
{
  EFI_STATUS        Status = EFI_ABORTED;
  EFI_IMAGE_OUTPUT  *Blt = NULL;

  Status = HiiFont->GetGlyph (
                      HiiFont,
                      L'B',
                      NULL,
                      &Blt,
                      NULL
                      );
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Failed. - %r\n", __FUNCTION__, __LINE__, Status));
  } else {
    Gop->Blt (
          Gop,
          Blt->Image.Bitmap,
          EfiBltBufferToVideo,
          0,
          0,
          0,
          0,
          Blt->Width,
          Blt->Height,
          0
          );
  }
}

/**
  Show text mode.

  @param[in]  Stp                     Pointer to EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL.

  @retval  NA

**/
VOID
ShowTextMode (
  IN  EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL *Stp
  )
{
  EFI_STATUS  Status = EFI_ABORTED;
  UINTN       Index = 0;
  UINTN       Col = 0;
  UINTN       Row = 0;

  Print (L"Current Text Mode:\r\n");
  Print (L" MaxMode       : %d\r\n", Stp->Mode->MaxMode);
  Print (L" Mode          : %d\r\n", Stp->Mode->Mode);
  Print (L" Attribute     : 0x%x\r\n", Stp->Mode->Attribute);
  Print (L" CursorColumn  : %d\r\n", Stp->Mode->CursorColumn);
  Print (L" CursorRow     : %d\r\n", Stp->Mode->CursorRow);
  Print (L" CursorVisible : %d\r\n", Stp->Mode->CursorVisible);

  Print (L"Supported Text Mode:\r\n");
  for (Index = 0; Index < Stp->Mode->MaxMode; Index++) {
    Status = Stp->QueryMode (Stp, Index, &Col, &Row);
    if (EFI_ERROR (Status)) {
      Print (L"%d. Not supported.\r\n", Index);
      continue;
    }
    Print (L"%d. Column: %d, Row: %d\r\n", Index, Col, Row);
  }
}

/**
  Function for 'display' command.

  @param[in]  ImageHandle           The image handle.
  @param[in]  SystemTable           The system table.

  @retval  SHELL_SUCCESS            Command completed successfully.
  @retval  SHELL_INVALID_PARAMETER  Command usage error.
  @retval  SHELL_ABORTED            The user aborts the operation.
  @retval  Value                    Unknown error.

**/
SHELL_STATUS
RunDisplay (
  IN  EFI_HANDLE                    ImageHandle,
  IN  EFI_SYSTEM_TABLE              *SystemTable
  )
{
  SHELL_STATUS                    ShellStatus = SHELL_INVALID_PARAMETER;
  EFI_STATUS                      Status = EFI_ABORTED;
  LIST_ENTRY                      *CheckPackage = NULL;
  CHAR16                          *ProblemParam = NULL;
  UINTN                           ParamCount = 0;
  EFI_GRAPHICS_OUTPUT_PROTOCOL    *Gop = NULL;
  EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL *Stp = NULL;
  EFI_HII_FONT_PROTOCOL           *HiiFont = NULL;

  //
  // Initialize the Shell library (we must be in non-auto-init...).
  //
  Status = ShellInitialize ();
  if (EFI_ERROR (Status)) {
    return SHELL_ABORTED;
  }

  //
  // Parse the command line.
  //
  Status = ShellCommandLineParse (ParamList, &CheckPackage, &ProblemParam, TRUE);
  if (EFI_ERROR (Status)) {
    if ((Status == EFI_VOLUME_CORRUPTED) && (ProblemParam != NULL) ) {
      ShellPrintHiiEx (
        -1, -1, NULL, STRING_TOKEN (STR_GEN_PROBLEM),
        mDisplayHiiHandle, L"display", ProblemParam
        );
      FreePool (ProblemParam);
    }
    goto DONE;
  }

  //
  // Check the number of parameters
  //
  ParamCount = ShellCommandLineGetCount (CheckPackage);
  if (ParamCount > 2) {
    ShellPrintHiiEx (
      -1, -1, NULL, STRING_TOKEN (STR_GEN_TOO_MANY),
      mDisplayHiiHandle, L"display"
      );
    goto DONE;
  }

  Status = GetSpecificGop (&Gop, &Stp);
  if (EFI_ERROR (Status)) {
    Print (L"No video card found!\r\n");
    goto DONE;
  }

  Status = gBS->LocateProtocol (
                  &gEfiHiiFontProtocolGuid,
                  NULL,
                  (VOID **)&HiiFont
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Failed. - %r\n", __FUNCTION__, __LINE__, Status));
    //
    // Event if not found, we still can do something else.
    //
    HiiFont = NULL;
  }

  Print (L"*********************** ͼ�κ�������� ***********************\r\n");
  if (ShellCommandLineGetFlag (CheckPackage, L"mode")) {
    ShowGopMode (Gop);
  }

  if (ShellCommandLineGetFlag (CheckPackage, L"blink")) {
    ScreenBlink (Gop);
  }

  if (ShellCommandLineGetFlag (CheckPackage, L"fill")) {
    FillScreen (Gop);
  }

  if (ShellCommandLineGetFlag (CheckPackage, L"bar1")) {
    ShowBar1 (Gop);
  }

  if (ShellCommandLineGetFlag (CheckPackage, L"bar2")) {
    ShowBar2 (Gop);
  }

  if (ShellCommandLineGetFlag (CheckPackage, L"a")) {
    ShowA (Gop);
  }

  if (ShellCommandLineGetFlag (CheckPackage, L"b")) {
    if (NULL != HiiFont) {
      ShowB (HiiFont, Gop);
    } else {
      Print (L"ShowB Failed!");
    }
  }

  if (ShellCommandLineGetFlag (CheckPackage, L"font")) {
    ShowFont (Gop);
  }

  if (ShellCommandLineGetFlag (CheckPackage, L"textmode")) {
    ShowTextMode (Stp);
  }

DONE:

  if ((ShellStatus != SHELL_SUCCESS) && (EFI_ERROR (Status))) {
    ShellStatus = Status & ~MAX_BIT;
  }

  return ShellStatus;
}
