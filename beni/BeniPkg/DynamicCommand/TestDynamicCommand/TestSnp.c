/**
*  @Package     : BeniPkg
*  @FileName    : TestSnp.c
*  @Date        : 20230923
*  @Author      : Jiangwei
*  @Version     : 1.0
*  @Description :
*    SNP test.
*
*  @History:
*    20230923: Initialize.
*
*  This program and the accompanying materials
*  are licensed and made available under the terms and conditions of the BSD License
*  which accompanies this distribution. The full text of the license may be found at
*  http://opensource.org/licenses/bsd-license.php
*
*  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
*  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
**/

#include "Test.h"

/**
  SNP check.

  @param[in]  Handle                The handle SNP resides in.

  @return  NA

**/
VOID
CheckSnp (
  IN  EFI_HANDLE                    Handle
  )
{
  EFI_STATUS                  Status = EFI_ABORTED;
  EFI_DEVICE_PATH_PROTOCOL    *DevPathProtocol = NULL;
  EFI_SIMPLE_NETWORK_PROTOCOL *Snp = NULL;

  Status = gBS->HandleProtocol (
                Handle,
                &gEfiSimpleNetworkProtocolGuid,
                (VOID **)&Snp
                );
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Failed. - %r\n", __FUNCTION__, __LINE__, Status));
    return;
  }

  Status = gBS->HandleProtocol (
                  Handle,
                  &gEfiDevicePathProtocolGuid,
                  (VOID **)&DevPathProtocol
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Failed. - %r\n", __FUNCTION__, __LINE__, Status));
    return;
  }

  NET_DEBUG_ERROR ("TestSnp", ("This is a SNP test!\n"));

  BeniPrintDevicePath (DevPathProtocol);
}

/**
  Test SNP.

  @param  NA

  @return  NA

**/
VOID
TestSnp (
  VOID
  )
{
  EFI_STATUS  Status = EFI_ABORTED;
  EFI_HANDLE  *Handles = NULL;
  UINTN       HandleCount = 0;
  UINTN       Index = 0;

  Status = gBS->LocateHandleBuffer (
                  ByProtocol,
                  &gEfiSimpleNetworkProtocolGuid,
                  NULL,
                  &HandleCount,
                  &Handles
                  );
  if (!EFI_ERROR (Status)) {
    Print (L"%d SNP found.\r\n", HandleCount);
    for (Index = 0; Index < HandleCount; Index++) {
      CheckSnp (Handles[Index]);
    }
  } else {
    Print (L"No SNP found.\r\n");
  }

  if (NULL != Handles) {
    FreePool (Handles);
    Handles = NULL;
  }
}
