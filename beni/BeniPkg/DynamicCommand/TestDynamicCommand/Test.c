/**
*  @Package     : BeniPkg
*  @FileName    : Test.c
*  @Date        : 20220313
*  @Author      : Jiangwei
*  @Version     : 1.0
*  @Description :
*    This command is for test only.
*
*  @History:
*    20220313: Initialize.
*
*  This program and the accompanying materials
*  are licensed and made available under the terms and conditions of the BSD License
*  which accompanies this distribution. The full text of the license may be found at
*  http://opensource.org/licenses/bsd-license.php
*
*  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
*  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
**/

#include "Test.h"

STATIC CONST SHELL_PARAM_ITEM ParamList[] = {
  {L"asm",  TypeFlag},
  {L"json", TypeFlag},
  {L"dpc",  TypeFlag},
  {L"snp",  TypeFlag},
  {L"mnp",  TypeFlag},
  {L"arp",  TypeFlag},
  {L"tcp",  TypeFlag},
  {NULL,    TypeMax }
};

/**
  Function for 'test' command.

  @param[in]  ImageHandle           The image handle.
  @param[in]  SystemTable           The system table.

  @retval  SHELL_SUCCESS            Command completed successfully.
  @retval  SHELL_INVALID_PARAMETER  Command usage error.
  @retval  SHELL_ABORTED            The user aborts the operation.
  @retval  Value                    Unknown error.

**/
SHELL_STATUS
RunTest (
  IN  EFI_HANDLE                    ImageHandle,
  IN  EFI_SYSTEM_TABLE              *SystemTable
  )
{
  SHELL_STATUS  ShellStatus = SHELL_SUCCESS;
  EFI_STATUS    Status = EFI_ABORTED;
  LIST_ENTRY    *CheckPackage = NULL;
  CHAR16        *ProblemParam = NULL;
  CONST CHAR16  *Param1 = NULL;
  CONST CHAR16  *Param2 = NULL;

  //
  // Initialize the Shell library (we must be in non-auto-init...).
  //
  Status = ShellInitialize ();
  if (EFI_ERROR (Status)) {
    return SHELL_ABORTED;
  }

  //
  // Parse the command line.
  //
  Status = ShellCommandLineParse (ParamList, &CheckPackage, &ProblemParam, TRUE);
  if (EFI_ERROR (Status)) {
    if ((Status == EFI_VOLUME_CORRUPTED) && (ProblemParam != NULL) ) {
      ShellPrintHiiEx (
        -1, -1, NULL, STRING_TOKEN (STR_GEN_PROBLEM),
        mTestHiiHandle, L"test", ProblemParam
        );
      FreePool (ProblemParam);
    }
    goto DONE;
  }

  ShellPrintHiiEx (-1, -1, NULL, STRING_TOKEN (STR_TEST_START), mTestHiiHandle);
  if (ShellCommandLineGetFlag (CheckPackage, L"asm")) {
    TestAsm ();
  }
  if (ShellCommandLineGetFlag (CheckPackage, L"json")) {
    Param1 = ShellCommandLineGetRawValue (CheckPackage, 1);
    if (NULL == Param1) {
      Param1 = L"example.json";
    }
    TestJson (Param1);
  }
  if (ShellCommandLineGetFlag (CheckPackage, L"dpc")) {
    TestDpc ();
  }
  if (ShellCommandLineGetFlag (CheckPackage, L"snp")) {
    TestSnp ();
  }
  if (ShellCommandLineGetFlag (CheckPackage, L"mnp")) {
    TestMnp ();
  }
  if (ShellCommandLineGetFlag (CheckPackage, L"arp")) {
    Param1 = ShellCommandLineGetRawValue (CheckPackage, 1);
    if (NULL == Param1) {
      Status = EFI_INVALID_PARAMETER;
      goto DONE;
    }
    Param2 = ShellCommandLineGetRawValue (CheckPackage, 2);
    if (NULL == Param2) {
      Status = EFI_INVALID_PARAMETER;
      goto DONE;
    }
    TestArp (Param1, Param2);
  }
  if (ShellCommandLineGetFlag (CheckPackage, L"tcp")) {
    TestTcp ();
  }
  ShellPrintHiiEx (-1, -1, NULL, STRING_TOKEN (STR_TEST_END), mTestHiiHandle);

DONE:

  if (EFI_ERROR (Status)) {
    ShellStatus = Status & ~MAX_BIT;
  }

  return ShellStatus;
}
