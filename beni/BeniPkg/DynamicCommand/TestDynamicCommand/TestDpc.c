/**
*  @Package     : BeniPkg
*  @FileName    : TestDpc.c
*  @Date        : 20230923
*  @Author      : Jiangwei
*  @Version     : 1.0
*  @Description :
*    Assembly test.
*
*  @History:
*    20230923: Initialize.
*
*  This program and the accompanying materials
*  are licensed and made available under the terms and conditions of the BSD License
*  which accompanies this distribution. The full text of the license may be found at
*  http://opensource.org/licenses/bsd-license.php
*
*  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
*  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
**/

#include "Test.h"

/**
  DPC function.

  @param  NA

  @retval  NA

**/
VOID
EFIAPI
DpcCallback (
  IN  VOID                          *Context
  )
{
  Print (L"DPC callback function\r\n");
}

/**
  Test DpcLib code.

  @param  NA

  @return  NA

**/
VOID
TestDpc (
  VOID
  )
{
  QueueDpc (TPL_CALLBACK, DpcCallback, NULL);
}
