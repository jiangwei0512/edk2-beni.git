/**
*  @Package     : BeniPkg
*  @FileName    : Test.h
*  @Date        : 20220313
*  @Author      : Jiangwei
*  @Version     : 1.0
*  @Description :
*    This command is for test only.
*
*  @History:
*    20220313: Initialize.
*
*  This program and the accompanying materials
*  are licensed and made available under the terms and conditions of the BSD License
*  which accompanies this distribution. The full text of the license may be found at
*  http://opensource.org/licenses/bsd-license.php
*
*  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
*  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
**/

#ifndef __TEST_H__
#define __TEST_H__

#include <Uefi.h>

#include <Library/UefiLib.h>
#include <Library/DebugLib.h>
#include <Library/ShellLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/HiiLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/JsonLib.h>
#include <Library/DpcLib.h>
#include <Library/DevicePathLib.h>
#include <Library/NetLib.h>
#include <Library/PrintLib.h>
#include <Library/BeniAsmLib.h>
#include <Library/BeniHiiLib.h>
#include <Library/BeniDebugInfoLib.h>
#include <Library/BeniDevicePathLib.h>

#include <Protocol/HiiPackageList.h>
#include <Protocol/SimpleNetwork.h>
#include <Protocol/ManagedNetwork.h>
#include <Protocol/Arp.h>
#include <Protocol/DevicePath.h>
#include <Protocol/ServiceBinding.h>
#include <Protocol/ShellDynamicCommand.h>
#include <Protocol/TcpTransportProtocol.h>

//
// Used for shell display.
//
extern EFI_HII_HANDLE mTestHiiHandle;

/**
  Function for 'test' command.

  @param[in]  ImageHandle           The image handle.
  @param[in]  SystemTable           The system table.

  @retval  SHELL_SUCCESS            Command completed successfully.
  @retval  SHELL_INVALID_PARAMETER  Command usage error.
  @retval  SHELL_ABORTED            The user aborts the operation.
  @retval  Value                    Unknown error.

**/
SHELL_STATUS
RunTest (
  IN  EFI_HANDLE                    ImageHandle,
  IN  EFI_SYSTEM_TABLE              *SystemTable
  );

/**
  Test assembly code.

  @param  NA

  @return  NA

**/
VOID
TestAsm (
  VOID
  );

/**
  Test DpcLib code.

  @param  NA

  @return  NA

**/
VOID
TestDpc (
  VOID
  );

/**
  Test JSON code.

  @param[in]  Param                 The JOSN file name.

  @return  NA

**/
VOID
TestJson (
  IN  CONST CHAR16                  *Param
  );

/**
  Test SNP.

  @param  NA

  @return  NA

**/
VOID
TestSnp (
  VOID
  );

/**
  Test MNP.

  @param  NA

  @return  NA

**/
VOID
TestMnp (
  VOID
  );

/**
  Test ARP.

  @param[in]  SrcString             The source IP string.
  @param[in]  DstString             The destination IP string where to get MAC.

  @return  NA

**/
VOID
TestArp (
  IN CONST CHAR16                   *SrcString,
  IN CONST CHAR16                   *DstString
  );

/**
  Test TCP.

  @param  NA

  @return  NA

**/
VOID
TestTcp (
  VOID
  );

#endif // __TEST_H__
