/**
*  @Package     : BeniPkg
*  @FileName    : TestArp.c
*  @Date        : 20231205
*  @Author      : Jiangwei
*  @Version     : 1.0
*  @Description :
*    ARP test.
*
*  @History:
*    20231205: Initialize.
*
*  This program and the accompanying materials
*  are licensed and made available under the terms and conditions of the BSD License
*  which accompanies this distribution. The full text of the license may be found at
*  http://opensource.org/licenses/bsd-license.php
*
*  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
*  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
**/

#include "Test.h"

/**
  Notify the callback function when an event is triggered.

  @param[in]  Event                 The triggered event.
  @param[in]  Context               The opaque parameter to the function.

**/
VOID
EFIAPI
CheckIfResolved (
  IN  EFI_EVENT                     Event,
  IN  VOID                          *Context
  )
{
  *((BOOLEAN *)Context) = TRUE;
}

/**
  ARP check.

  @param[in]  Handle                The handle ARP resides in.
  @param[in]  SrcString             The source IP string.
  @param[in]  DstString             The destination IP string where to get MAC.

  @return  NA

**/
VOID
CheckArp (
  IN  EFI_HANDLE                    Handle,
  IN CONST CHAR16                   *SrcString,
  IN CONST CHAR16                   *DstString
  )
{
  EFI_STATUS          Status = EFI_ABORTED;
  EFI_ARP_PROTOCOL    *Arp = NULL;
  EFI_HANDLE          ArpHandle = NULL;
  EFI_IPv4_ADDRESS    SrcIp;
  EFI_IPv4_ADDRESS    DestIp;
  IP4_ADDR            IpAddr;
  EFI_MAC_ADDRESS     Mac;
  EFI_MAC_ADDRESS     ZeroMac;
  EFI_ARP_CONFIG_DATA ArpConfig;
  EFI_EVENT           ResolvedEvent;
  BOOLEAN             IsResolved = FALSE;

  ZeroMem (&Mac, sizeof (EFI_MAC_ADDRESS));
  ZeroMem (&ZeroMac, sizeof (EFI_MAC_ADDRESS));

  Print (L"Resolving IP %s ...\r\n", DstString);

  Status = NetLibCreateServiceChild (
            Handle,
            Handle,
            &gEfiArpServiceBindingProtocolGuid,
            &ArpHandle
            );
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Failed. - %r\n", __FUNCTION__, __LINE__, Status));
    return;
  }

  Status = gBS->OpenProtocol (
                  ArpHandle,
                  &gEfiArpProtocolGuid,
                  (VOID **)(&Arp),
                  Handle,
                  Handle,
                  EFI_OPEN_PROTOCOL_BY_DRIVER
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Failed. - %r\n", __FUNCTION__, __LINE__, Status));
    return;
  }

  Status = NetLibStrToIp4 (SrcString, &SrcIp);
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Failed. - %r\n", __FUNCTION__, __LINE__, Status));
    return;
  } else {
    Print (L"Source IP     : %d.%d.%d.%d\r\n",
            SrcIp.Addr[0],
            SrcIp.Addr[1],
            SrcIp.Addr[2],
            SrcIp.Addr[3]
            );
  }

  Status = NetLibStrToIp4 (DstString, &DestIp);
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Failed. - %r\n", __FUNCTION__, __LINE__, Status));
    return;
  } else {
    Print (L"Destination IP: %d.%d.%d.%d\r\n",
            DestIp.Addr[0],
            DestIp.Addr[1],
            DestIp.Addr[2],
            DestIp.Addr[3]
            );
  }

  IpAddr                    = EFI_NTOHL (SrcIp);
  IpAddr                    = HTONL (IpAddr);
  ArpConfig.SwAddressType   = 0x0800;
  ArpConfig.SwAddressLength = 4;
  ArpConfig.StationAddress  = &IpAddr;
  ArpConfig.EntryTimeOut    = 0;
  ArpConfig.RetryCount      = 0;
  ArpConfig.RetryTimeOut    = 0;

  Status = Arp->Configure (Arp, NULL);
  Status = Arp->Configure (Arp, &ArpConfig);
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Failed. - %r\n", __FUNCTION__, __LINE__, Status));
    return;
  }

  Status = gBS->CreateEvent (
                  EVT_NOTIFY_SIGNAL,
                  TPL_NOTIFY,
                  CheckIfResolved,
                  &IsResolved,
                  &ResolvedEvent
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Failed. - %r\n", __FUNCTION__, __LINE__, Status));
    return;
  }

  Status = Arp->Request (Arp, &DestIp, ResolvedEvent, &Mac);
  if (EFI_ERROR (Status) && (Status != EFI_NOT_READY)) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Failed. - %r\n", __FUNCTION__, __LINE__, Status));
    return;
  }

  while (!IsResolved) {
    if (CompareMem (&Mac, &ZeroMac, sizeof (EFI_MAC_ADDRESS)) != 0) {
      break;
    }
  }

  Print (L"MAC: %02x:%02x:%02x:%02x:%02x:%02x\r\n",
          Mac.Addr[0],
          Mac.Addr[1],
          Mac.Addr[2],
          Mac.Addr[3],
          Mac.Addr[4],
          Mac.Addr[5]
          );
}

/**
  Test ARP.

  @param[in]  SrcString             The source IP string.
  @param[in]  DstString             The destination IP string where to get MAC.

  @return  NA

**/
VOID
TestArp (
  IN CONST CHAR16                   *SrcString,
  IN CONST CHAR16                   *DstString
  )
{
  EFI_STATUS  Status = EFI_ABORTED;
  EFI_HANDLE  *Handles = NULL;
  UINTN       HandleCount = 0;
  UINTN       Index = 0;

  Status = gBS->LocateHandleBuffer (
                  ByProtocol,
                  &gEfiArpServiceBindingProtocolGuid,
                  NULL,
                  &HandleCount,
                  &Handles
                  );
  if (!EFI_ERROR (Status)) {
    Print (L"%d ARP found.\r\n", HandleCount);
    for (Index = 0; Index < HandleCount; Index++) {
      CheckArp (Handles[Index], SrcString, DstString);
    }
  } else {
    Print (L"No ARP found.\r\n");
  }

  if (NULL != Handles) {
    FreePool (Handles);
    Handles = NULL;
  }
}
