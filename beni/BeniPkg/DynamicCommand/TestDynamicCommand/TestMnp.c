/**
*  @Package     : BeniPkg
*  @FileName    : TestMnp.c
*  @Date        : 20231001
*  @Author      : Jiangwei
*  @Version     : 1.0
*  @Description :
*    MNP test.
*
*  @History:
*    20231001: Initialize.
*    20231221: Modification.
*
*  This program and the accompanying materials
*  are licensed and made available under the terms and conditions of the BSD License
*  which accompanies this distribution. The full text of the license may be found at
*  http://opensource.org/licenses/bsd-license.php
*
*  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
*  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
**/

#include "Test.h"

GLOBAL_REMOVE_IF_UNREFERENCED UINTN   mMnpNetDebugLevelMax = NETDEBUG_LEVEL_ERROR;
GLOBAL_REMOVE_IF_UNREFERENCED UINT32  mMnpSyslogPacketSeq  = 0xDEADBEEF;
GLOBAL_REMOVE_IF_UNREFERENCED CHAR8   *mMnpMonthName[] = {
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec"
};

/**
  Locate the handles that support MNP, then open one of them
  to send the syslog packets. The caller isn't required to close
  the MNP after use because the MNP is opened by HandleProtocol.

  @param  NA

  @retval EFI_MANAGED_NETWORK_PROTOCOL* The point to MNP.

**/
EFI_MANAGED_NETWORK_PROTOCOL *
SyslogLocateMnp (
  VOID
  )
{
  EFI_MANAGED_NETWORK_PROTOCOL  *Mnp = NULL;
  EFI_HANDLE                    MnpHandle = NULL;
  EFI_STATUS                    Status = EFI_ABORTED;
  EFI_HANDLE                    *Handles = NULL;
  UINTN                         HandleCount = 0;
  UINTN                         Index = 0;

  //
  // Locate the handles which has MNS service installed.
  //
  Handles = NULL;
  Status  = gBS->LocateHandleBuffer (
                  ByProtocol,
                  &gEfiManagedNetworkServiceBindingProtocolGuid,
                  NULL,
                  &HandleCount,
                  &Handles
                  );
  if (EFI_ERROR (Status) || (HandleCount == 0)) {
    return NULL;
  }

  //
  // Try to open one of the MNP protocol to send packet.
  //
  for (Index = 0; Index < HandleCount; Index++) {
    Mnp = NULL;
    //
    // This should be destroyed using NetLibDestroyServiceChild()
    // Well...It's just a test...I'm tried to do this.
    //
    Status = NetLibCreateServiceChild (
              Handles[Index],
              Handles[Index],
              &gEfiManagedNetworkServiceBindingProtocolGuid,
              &MnpHandle
              );
    if (!EFI_ERROR (Status)) {
      Status = gBS->OpenProtocol (
                      MnpHandle,
                      &gEfiManagedNetworkProtocolGuid,
                      (VOID **)(&Mnp),
                      Handles[Index],
                      Handles[Index],
                      EFI_OPEN_PROTOCOL_BY_DRIVER
                      );
    }
    if (!EFI_ERROR (Status) && (NULL != Mnp)) {
      break;
    }
  }

  FreePool (Handles);

  return Mnp;
}

/**
  SysLog has benn sent.

  @param[in]  Event                 The Event this notify function registered to.
  @param[in]  Context               Pointer to the context data registered to the
                                    Event.

  @return  NA

**/
VOID
EFIAPI
SysLogSent (
  IN  EFI_EVENT                     Event,
  IN  VOID                          *Context
  )
{
  DEBUG ((EFI_D_ERROR, "SysLog sent\n"));
}

/**
  Transmit a syslog packet synchronously through SNP. The Packet
  already has the ethernet header prepended. This function should
  fill in the source MAC because it will try to locate a SNP each
  time it is called to avoid the problem if SNP is unloaded.
  This code snip is copied from MNP.

  @param[in]  Packet                The Syslog packet.
  @param[in]  Length                The length of the packet.

  @retval  EFI_DEVICE_ERROR         Failed to locate a usable SNP protocol.
  @retval  EFI_TIMEOUT              Timeout happened to send the packet.
  @retval  EFI_SUCCESS              Packet is sent.

**/
EFI_STATUS
MnpSyslogSendPacket (
  IN  CHAR8                         *Packet,
  IN  UINT32                        Length
  )
{
  EFI_MANAGED_NETWORK_PROTOCOL          *Mnp = NULL;
  ETHER_HEAD                            *Ether = NULL;
  EFI_STATUS                            Status = EFI_ABORTED;
  EFI_SIMPLE_NETWORK_MODE               SnpMode;
  EFI_MANAGED_NETWORK_COMPLETION_TOKEN  *TxToken = NULL;
  EFI_MANAGED_NETWORK_TRANSMIT_DATA     *TxData = NULL;
  EFI_MANAGED_NETWORK_CONFIG_DATA       MnpConfigData;

  if ((NULL == Packet) || (0 == Length)) {
    return EFI_INVALID_PARAMETER;
  }

  ZeroMem (&SnpMode, sizeof (EFI_SIMPLE_NETWORK_MODE));
  ZeroMem (&MnpConfigData, sizeof (EFI_MANAGED_NETWORK_CONFIG_DATA));

  Mnp = SyslogLocateMnp ();
  if (NULL == Mnp) {
    return EFI_NOT_FOUND;
  }

  MnpConfigData.ReceivedQueueTimeoutValue = 0;
  MnpConfigData.TransmitQueueTimeoutValue = 0;
  MnpConfigData.ProtocolTypeFilter        = 0x0800;
  MnpConfigData.EnableUnicastReceive      = TRUE;
  MnpConfigData.EnableMulticastReceive    = TRUE;
  MnpConfigData.EnableBroadcastReceive    = TRUE;
  MnpConfigData.EnablePromiscuousReceive  = FALSE;
  MnpConfigData.FlushQueuesOnReset        = TRUE;
  MnpConfigData.EnableReceiveTimestamps   = FALSE;
  MnpConfigData.DisableBackgroundPolling  = FALSE;

  Status = Mnp->Configure (Mnp, NULL);
  Status = Mnp->Configure (Mnp, &MnpConfigData);
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Failed. - %r\n", __FUNCTION__, __LINE__, Status));
    return Status;
  }

  Status = Mnp->GetModeData (Mnp, NULL, &SnpMode);
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "GetModeData failed - %r\n", Status));
    return Status;
  }

  DEBUG ((EFI_D_ERROR, "MAC: %02x:%02x:%02x:%02x:%02x:%02x\n",
          SnpMode.CurrentAddress.Addr[0],
          SnpMode.CurrentAddress.Addr[1],
          SnpMode.CurrentAddress.Addr[2],
          SnpMode.CurrentAddress.Addr[3],
          SnpMode.CurrentAddress.Addr[4],
          SnpMode.CurrentAddress.Addr[5]
          ));

  Ether = (ETHER_HEAD *)Packet;
  CopyMem (Ether->SrcMac, SnpMode.CurrentAddress.Addr, NET_ETHER_ADDR_LEN);

  TxToken = AllocateZeroPool (sizeof (EFI_MANAGED_NETWORK_COMPLETION_TOKEN));
  if (NULL == TxToken) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Out of memory\n", __FUNCTION__, __LINE__));
    return EFI_OUT_OF_RESOURCES;
  }

  //
  // Create the event for this TxToken.
  //
  Status = gBS->CreateEvent (
                  EVT_NOTIFY_SIGNAL,
                  TPL_NOTIFY,
                  SysLogSent,
                  (VOID *)TxToken,
                  &TxToken->Event
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "ArpSendFrame: CreateEvent failed for TxToken->Event.\n"));
    goto DONE;
  }

  TxData = AllocatePool (sizeof (EFI_MANAGED_NETWORK_TRANSMIT_DATA));
  if (NULL == TxData) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Out of memory\n", __FUNCTION__, __LINE__));
    return EFI_OUT_OF_RESOURCES;
  }

  //
  // Set all the fields of the TxData.
  //
  TxData->DestinationAddress = NULL;
  TxData->SourceAddress      = NULL;
  TxData->ProtocolType       = 0x0800;
  TxData->DataLength         = Length - SnpMode.MediaHeaderSize;  // DataLength + HeaderLength
  TxData->HeaderLength       = (UINT16)SnpMode.MediaHeaderSize;   // = FragmentLength
  TxData->FragmentCount      = 1;

  TxData->FragmentTable[0].FragmentBuffer = Packet; // The real data to transmit.
  TxData->FragmentTable[0].FragmentLength = Length;

  TxToken->Packet.TxData = TxData;
  TxToken->Status        = EFI_NOT_READY;

  Status = Mnp->Transmit (Mnp, TxToken);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Mnp->Transmit failed -%r\n", Status));
    goto DONE;
  }

DONE:

  if (NULL != TxToken->Event) {
    gBS->CloseEvent (TxToken->Event);
  }

  if (NULL != TxData) {
    FreePool (TxData);
  }

  if (NULL != TxToken) {
    FreePool (TxToken);
  }

  return Status;
}

/**
  Build a syslog packet, including the Ethernet/Ip/Udp headers
  and user's message.

  @param[in]  Level                 Syslog severity level
  @param[in]  Module                The module that generates the log
  @param[in]  File                  The file that contains the current log
  @param[in]  Line                  The line of code in the File that contains the current log
  @param[in]  Message               The log message
  @param[in]  BufLen                The length of the Buf
  @param[out] Buf                   The buffer to put the packet data

  @retval  UINT32                   The length of the syslog packet built,
                                    0 represents no packet is built.

**/
UINT32
MnpSyslogBuildPacket (
  IN  UINT32                        Level,
  IN  UINT8                         *Module,
  IN  UINT8                         *File,
  IN  UINT32                        Line,
  IN  UINT8                         *Message,
  IN  UINT32                        BufLen,
  OUT CHAR8                         *Buf
  )
{
  EFI_STATUS      Status = EFI_ABORTED;
  ETHER_HEAD      *Ether = NULL;
  IP4_HEAD        *Ip4 = NULL;
  EFI_UDP_HEADER  *Udp4 = NULL;
  EFI_TIME        Time;
  UINT32          Pri = 0;
  UINT32          Len = 0;
  UINT8           SyslogDstMac[NET_ETHER_ADDR_LEN] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};

  //
  // Fill in the Ethernet header. Leave alone the source MAC.
  // MnpSyslogSendPacket will fill in the address for us.
  //
  Ether = (ETHER_HEAD *)Buf;
  CopyMem (Ether->DstMac, SyslogDstMac, NET_ETHER_ADDR_LEN);
  ZeroMem (Ether->SrcMac, NET_ETHER_ADDR_LEN);
  ZeroMem (&Time, sizeof (EFI_TIME));

  Ether->EtherType = HTONS (0x0800);  // IPv4 protocol

  Buf    += sizeof (ETHER_HEAD);
  BufLen -= sizeof (ETHER_HEAD);

  //
  // Fill in the IP header.
  //
  Ip4           = (IP4_HEAD *)Buf;
  Ip4->HeadLen  = 5;
  Ip4->Ver      = 4;
  Ip4->Tos      = 0;
  Ip4->TotalLen = 0;
  Ip4->Id       = (UINT16)mMnpSyslogPacketSeq;
  Ip4->Fragment = 0;
  Ip4->Ttl      = 16;
  Ip4->Protocol = 0x11;
  Ip4->Checksum = 0;
  Ip4->Src      = 0;
  Ip4->Dst      = 0xffffffff;

  Buf    += sizeof (IP4_HEAD);
  BufLen -= sizeof (IP4_HEAD);

  //
  // Fill in the UDP header, Udp checksum is optional. Leave it zero.
  //
  Udp4           = (EFI_UDP_HEADER *)Buf;
  Udp4->SrcPort  = HTONS (514);
  Udp4->DstPort  = HTONS (514);
  Udp4->Length   = 0;
  Udp4->Checksum = 0;

  Buf    += sizeof (EFI_UDP_HEADER);
  BufLen -= sizeof (EFI_UDP_HEADER);

  //
  // Build the syslog message body with <PRI> Timestamp machine module Message.
  //
  Pri    = ((NET_SYSLOG_FACILITY & 31) << 3) | (Level & 7);
  Status = gRT->GetTime (&Time, NULL);
  if (EFI_ERROR (Status)) {
    return 0;
  }

  //
  // Use %a to format the ASCII strings, %s to format UNICODE strings.
  //
  Len  = 0;
  Len += (UINT32)AsciiSPrint (
                   Buf,
                   BufLen,
                   "<%d> %a %d %d:%d:%d ",
                   Pri,
                   mMnpMonthName[Time.Month-1],
                   Time.Day,
                   Time.Hour,
                   Time.Minute,
                   Time.Second
                   );

  Len += (UINT32)AsciiSPrint (
                   Buf + Len,
                   BufLen - Len,
                   "Tiano %a: %a (Line: %d File: %a)",
                   Module,
                   Message,
                   Line,
                   File
                   );
  Len++;

  //
  // OK, patch the IP length/checksum and UDP length fields.
  //
  Len         += sizeof (EFI_UDP_HEADER);
  Udp4->Length = HTONS ((UINT16)Len);

  Len          += sizeof (IP4_HEAD);
  Ip4->TotalLen = HTONS ((UINT16)Len);
  Ip4->Checksum = (UINT16)(~NetblockChecksum ((UINT8 *)Ip4, sizeof (IP4_HEAD)));

  return Len + sizeof (ETHER_HEAD);
}

/**
  Builds an UDP4 syslog packet and send it using SNP.

  This function will locate a instance of SNP then send the message through it.
  Because it isn't open the SNP BY_DRIVER, apply caution when using it.

  @param[in]  Level                 The severity level of the message.
  @param[in]  Module                The Module that generates the log.
  @param[in]  File                  The file that contains the log.
  @param[in]  Line                  The exact line that contains the log.
  @param[in]  Message               The user message to log.

  @retval  EFI_INVALID_PARAMETER    Any input parameter is invalid.
  @retval  EFI_OUT_OF_RESOURCES     Failed to allocate memory for the packet.
  @retval  EFI_DEVICE_ERROR         Device error occurs.
  @retval  EFI_SUCCESS              The log is discard because that it is more verbose
                                    than the mMnpNetDebugLevelMax. Or, it has been sent out.
**/
EFI_STATUS
MnpNetDebugOutput (
  IN  UINT32                        Level,
  IN  UINT8                         *Module,
  IN  UINT8                         *File,
  IN  UINT32                        Line,
  IN  UINT8                         *Message
  )
{
  CHAR8       *Packet = NULL;
  UINT32      Len = 0;
  EFI_STATUS  Status = EFI_ABORTED;

  //
  // Check whether the message should be sent out.
  //
  if ((Message == NULL) || (File == NULL) || (Module == NULL)) {
    return EFI_INVALID_PARAMETER;
  }

  if (Level > mMnpNetDebugLevelMax) {
    Status = EFI_SUCCESS;
    goto ON_EXIT;
  }

  //
  // Allocate a maximum of 1024 bytes, the caller should ensure
  // that the message plus the ethernet/ip/udp header is shorter
  // than this.
  //
  Packet = (CHAR8 *)AllocatePool (NET_SYSLOG_PACKET_LEN);
  if (Packet == NULL) {
    Status = EFI_OUT_OF_RESOURCES;
    goto ON_EXIT;
  }

  //
  // Build the message: Ethernet header + IP header + Udp Header + user data
  //
  Len = MnpSyslogBuildPacket (
          Level,
          Module,
          File,
          Line,
          Message,
          NET_SYSLOG_PACKET_LEN,
          Packet
          );
  if (Len == 0) {
    Status = EFI_DEVICE_ERROR;
  } else {
    mMnpSyslogPacketSeq++;
    Status = MnpSyslogSendPacket (Packet, Len);
  }

  FreePool (Packet);

ON_EXIT:

  FreePool (Message);

  return Status;
}

#define MNP_NET_DEBUG_ERROR(Module, PrintArg) \
  MnpNetDebugOutput ( \
    NETDEBUG_LEVEL_ERROR, \
    Module, \
    __FILE__, \
    DEBUG_LINE_NUMBER, \
    NetDebugASPrint PrintArg \
    )

/**
  Test MNP.

  @param  NA

  @return  NA

**/
VOID
TestMnp (
  VOID
  )
{
  MNP_NET_DEBUG_ERROR ("TestMnp", ("This is a MNP test!\n"));
}
