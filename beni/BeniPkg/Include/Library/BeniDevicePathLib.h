/**
*  @Package     : BeniPkg
*  @FileName    : BeniDevicePathLib.h
*  @Date        : 20230923
*  @Author      : Jiangwei
*  @Version     : 1.0
*  @Description :
*    This is a library for deivce path.
*
*  @History:
*    20230923: Initialize.
*
*  This program and the accompanying materials
*  are licensed and made available under the terms and conditions of the BSD License
*  which accompanies this distribution. The full text of the license may be found at
*  http://opensource.org/licenses/bsd-license.php
*
*  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
*  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
**/

#ifndef __BENI_DEVICE_PATH_LIB_H__
#define __BENI_DEVICE_PATH_LIB_H__

#include <Uefi.h>

#include <Protocol/DevicePath.h>

/**
  Print device path.

  @param[in]  DevicePath            The device path protocol.

  @retval  NA

**/
VOID
EFIAPI
BeniPrintDevicePath (
  IN  EFI_DEVICE_PATH_PROTOCOL      *DevicePath
  );

#endif // __BENI_DEVICE_PATH_LIB_H__
