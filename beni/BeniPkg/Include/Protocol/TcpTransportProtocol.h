/**
*  @Package     : BeniPkg
*  @FileName    : TcpTransportProtocol.h
*  @Date        : 20231228
*  @Author      : Jiangwei
*  @Version     : 1.0
*  @Description :
*    This file provides TCP transport protocol.
*    Base on EmbeddedPkg\Include\Protocol\AndroidFastbootTransport.h.
*
*  @History:
*    20231228: Initialize.
*
*  This program and the accompanying materials
*  are licensed and made available under the terms and conditions of the BSD License
*  which accompanies this distribution. The full text of the license may be found at
*  http://opensource.org/licenses/bsd-license.php
*
*  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
*  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
**/

/** @file

  Copyright (c) 2014, ARM Ltd. All rights reserved.<BR>

  SPDX-License-Identifier: BSD-2-Clause-Patent

**/

#ifndef __TCP_TRANSPORT_H__
#define __TCP_TRANSPORT_H__

#include <Uefi.h>

//
// {41DCA89F-DBB4-488E-83C0-C61315DF5AEC}
//
#define TCP_TRANSPORT_PROTOCOL_GUID \
  { \
    0x41dca89f, 0xdbb4, 0x488e, { 0x83, 0xc0, 0xc6, 0x13, 0x15, 0xdf, 0x5a, 0xec} \
  }

#define TCP_TRANSPORT_PROTOCOL_REVISION_1_0 0x00010000

typedef struct _TCP_TRANSPORT_PROTOCOL  TCP_TRANSPORT_PROTOCOL;

extern EFI_GUID  gTcpTransportProtocolGuid;

/*
  Set up the transport system for use by Fastboot.
  e.g. For USB this probably means making the device enumerable. For TCP,
       preparing to accept incoming connections.

  It is _not_ the responsibility of this protocol's implementer to unite the
  data phase into a single buffer - that is handled by the Fastboot UEFI
  application. As the Fastboot protocol spec says: "Short packets are always
   acceptable and zero-length packets are ignored."
  However the commands and responses must be in a single packet, and the order
  of the packets must of course be maintained.

  If there is a fatal error in the receive channel, ReceiveEvent will be
  signalled, and a subsequent call to Receive() will return an error. This
  allows data transported prior to the error to be received.

  @param[in]  ReceiveEvent          Event to be Signalled when a packet has been received
                                    and is ready to be retrieved via Receive().

  @retval  EFI_SUCCESS              Initialised successfully.
  @retval  EFI_DEVICE_ERROR         Error in initialising hardware
  @retval  Others                   Error return from LocateProtocol functions.

*/
typedef
EFI_STATUS
(EFIAPI *TCP_TRANSPORT_START) (
  IN  EFI_EVENT                     ReceiveEvent
  );

/*
  Function to be called when all Fastboot transactions are finished, to
  de-initialise the transport system.
  e.g. A USB OTG system might want to get out of peripheral mode so it can be
       a USB host.

  Note that this function will be called after an error is reported by Send or
  Receive

  @param  NA

  @retval  EFI_SUCCESS              De-initialised successfully.
  @retval  EFI_DEVICE_ERROR         Error de-initialising hardware.

*/
typedef
EFI_STATUS
(EFIAPI *TCP_TRANSPORT_STOP) (
  VOID
  );

/*
  Send data. This function can be used both for command responses like "OKAY"
  and for the data phase (the protocol doesn't describe any situation when the
   latter might be necessary, but does allow it)

  Transmission need not finish before the function returns.
  If there is an error in transmission from which the transport system cannot
  recover, FatalErrorEvent will be signalled. Otherwise, it is assumed that all
  data was delivered successfully.

  @param[in]  BufferSize            Size in bytes of data to send.
  @param[in]  Buffer                Data to send.
  @param[in]  FatalErrorEvent       Event to signal if there was an error in
                                    transmission from which the transport system
                                    cannot recover.

  @retval  EFI_SUCCESS              The data was sent or queued for send.
  @retval  EFI_DEVICE_ERROR         There was an error preparing to send the data.

 */
typedef
EFI_STATUS
(EFIAPI *TCP_TRANSPORT_SEND) (
  IN  UINTN                         BufferSize,
  IN  CONST VOID                    *Buffer,
  IN  EFI_EVENT                     *FatalErrorEvent
  );

/*
  When the event has been Signalled to say data is available from the host,
  this function is used to get data. In order to handle the case where several
  packets are received before ReceiveEvent's notify function is called, packets
  received are queued, and each call to this function returns the next packet in
  the queue. It should therefore be called in a loop, the exit condition being a
  return of EFI_NOT_READY.

  @param[out]  Buffer               Pointer to received data. Callee allocated - the
                                    caller must free it with FreePool.
  @param[out]  BufferSize           The size of received data in bytes

  @retval  EFI_NOT_READY            There is no data available
  @retval  EFI_DEVICE_ERROR         There was a fatal error in the receive channel.
                                    e.g. for USB the cable was unplugged or for TCP the
                                    connection was closed by the remote host..

*/
typedef
EFI_STATUS
(EFIAPI *TCP_TRANSPORT_RECEIVE) (
  OUT UINTN                         *BufferSize,
  OUT VOID                          **Buffer
  );

struct _TCP_TRANSPORT_PROTOCOL {
  //
  // The revision to which the interface adheres. All future
  // revisions must be backwards compatible. If a future version is not
  // back wards compatible, it is not the same GUID.
  //
  UINT64                Revision;

  TCP_TRANSPORT_START   Start;
  TCP_TRANSPORT_STOP    Stop;
  TCP_TRANSPORT_SEND    Send;
  TCP_TRANSPORT_RECEIVE Receive;
};

#endif
