::::
:: @Package     : edk-beni
:: @FileName    : Build.cmd
:: @Date        : 20240302
:: @Author      : Jiangwei
:: @Version     : 1.0
:: @Description :
::   This is used to sign file.
::
:: @History:
::   20240302: Initialize.
::
:: This program and the accompanying materials
:: are licensed and made available under the terms and conditions of the BSD License
:: which accompanies this distribution. The full text of the license may be found at
:: http://opensource.org/licenses/bsd-license.php
::
:: THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
:: WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
::::

@echo off

set CUR_DIR=%CD%
set FILE_NAME=%1
set SIGN_DATA=sign.bin
set FILE_SINGED=%~n1_signed%~x1

%CUR_DIR%/openssl.exe dgst -sign %CUR_DIR%/private.pem -sha256 -out %SIGN_DATA% %CUR_DIR%/%1
copy /b %FILE_NAME%+%SIGN_DATA% %FILE_SINGED%

if exist %SIGN_DATA%  del /Q %SIGN_DATA%
